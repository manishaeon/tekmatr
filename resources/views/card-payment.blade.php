<!doctype html>
<html lang="en" dir="ltr">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>tek matr</title>
    <meta name="csrf-token" content="{{ csrf_token() }}" />


    <link rel="shortcut icon" href="{{ asset('images/tek-logo.png') }}" />

    <!-- Library / Plugin Css Build -->
    <link rel="stylesheet" href="{{ asset('css/core/libs.min.css') }}">

    <!-- Custom Css -->
    <link rel="stylesheet" href="{{ asset('css/aprycot.css') }}">
    <link rel="stylesheet" href="{{ asset('css/jquery-ui.min.css') }}">

    <script src="https://code.jquery.com/jquery-3.6.1.min.js"
        integrity="sha256-o88AwQnZB+VDvE9tvIXrMQaPlFFSUTR+nldQm1LuPXQ=" crossorigin="anonymous"></script>
        <script src="https://code.jquery.com/jquery-3.6.4.min.js"></script>

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.2.0/css/all.min.css"
        integrity="sha512-xh6O/CkQoPOWDdYTDqeRdPCVd1SpvCA9XXcUnZS2FmJNp1coAFzvtCN9BmamE+4aHK8yyUHUSCcJHgXloTyT2A=="
        crossorigin="anonymous" referrerpolicy="no-referrer" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.2.0/js/all.min.js"
        integrity="sha512-naukR7I+Nk6gp7p5TMA4ycgfxaZBJ7MO5iC3Fp6ySQyKFHOGfpkSZkYVWV5R7u7cfAicxanwYQ5D1e17EfJcMA=="
        crossorigin="anonymous" referrerpolicy="no-referrer"></script>

        <script src="https://js.stripe.com/v3/"></script>

    {{-- Sweet Alert --}}
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    
    <div id='loader'>    
    </div>
    <style>
        #loader {
        display: none;
        position: fixed;
        top: 0;
        left: 0;
        right: 0;
        bottom: 0;
        width: 100%;
        background:url({{ asset('img/loader.gif') }}) no-repeat center center rgba(0,0,0,0.50);      
        background-size: 150px;
        z-index: 99999;
    }
        .errorText{
            color: #cd1000;
            font-size: 12px;
        }
        .wp-width:{
            width: 25px;
        }
        @media screen and (min-device-width: 481px) and (max-device-width: 768px)
        {
            .wp-width{
                Width: 100%;
            }
        }
        .input-containersss {
            position: relative;
            }
            .currency-symbol {
            position: absolute;
            top: 54%;
            left: 7px;
            }

        #card-element {
            border: 1px solid #559e73;
            border-radius: 10px;
            padding: 0.75rem 1rem;
            margin-bottom:15px
        }
        
    </style>
    <style>
      #usaOptgroup {
          display: none;
      }
    </style>
</head>
<body style="background-color: #e5e5e5;">
    <section class="p-4 p-md-12" style="">
  <div class="row d-flex justify-content-center">
    <div class="col-md-12 col-lg-12 col-xl-9">
      <div class="card rounded-3" style="background: rgb(255 255 255);">
        <div class="card-body p-4">
          <div class="text-center mb-4">
            <img src="{{asset('images/tek-logo.png')}}" alt="" style="height: 70px;">
          </div>
          <form id="paymentform" novalidate>
            @csrf
            <div class="row">
            <div class="col-lg-12">
                @if (session('message'))
                    <div class="alert alert-success">
                        {{ session('message') }}
                    </div>
                @endif
                @if (session('error'))
                    <div class="alert alert-danger">
                        {{ session('error') }}
                    </div>
                @endif
            </div>
            <div class="row">
              <div id="error-message" class="error"></div>
            </div>
            <div class="col-md-6">
                <p class="fw-bold mb-1 pb-2">Card Details :</p>
                <div class="row">
					  <div class="col-md-6 col-12">
						<div class="form-outline mb-2 input-containersss">
						<label class="form-label">Pay Amount ($) <span class="text-danger">*</span></label>
						<input type="number" name="order_amount" class="form-control pricess form-control-md"
						placeholder="0.00" style="border-radius:10px;" aria-label="Amount (to the nearest dollar)" step=".01" value="{{old('order_amount')}}" required />
						@if ($errors->has('order_amount'))
							<span class="help-block errorText" style="font-size: 11px; white-space: nowrap;">
							{{ $errors->first('order_amount') }}
							</span>
						@endif
						</div>
					</div>
					
					<div class="col-md-6 col-12">
						<div class="form-outline mb-3">
						  <label class="form-label">Cardholder's Name <span class="text-danger">*</span></label>
						  <input type="text" name="card_holder_name"  class="form-control form-control-md"
						  placeholder="Cardholder's Name" style="border-radius:10px;" value="{{old('card_holder_name')}}" required/>
						@if ($errors->has('card_holder_name'))
						  <span class="help-block errorText" style="font-size: 11px; white-space: nowrap;">
						  {{ $errors->first('card_holder_name') }}
						  </span>
						@endif
						</div>
					</div>
				</div>
				<div class="row col-md-12 mx-1">
        <label class="form-label px-0">Card Number, Expiration, CVV <span class="text-danger">*</span></label>
					<div class=" form-control form-control-sm px-4" id="card-element" >
					
					</div>
        </div>
          <div class="row">            
            <div class="col-md-6 col-12">
              <div class="form-outline mb-3">
                <label class="form-label">First Name <span class="text-danger">*</span></label>
                <input type="text" name="first_name" class="form-control form-control-md"
                placeholder="First name" style="border-radius:10px;" value="{{old('first_name')}}" />
                @if ($errors->has('first_name'))
                <span class="help-block errorText">
                {{ $errors->first('first_name') }}
                </span>
                @endif
              </div>
            </div>
            <div class="col-md-6 col-12">
              <div class="form-outline mb-3">
                <label class="form-label">Last Name <span class="text-danger">*</span></label>
                <input type="text" name="last_name" class="form-control form-control-md"
                placeholder="Last name" style="border-radius:10px;" value="{{old('last_name')}}" />
                @if ($errors->has('last_name'))
                <span class="help-block errorText">
                {{ $errors->first('last_name') }}
                </span>
                @endif
              </div>
            </div>
            <div class="col-md-6 col-12">
                      <div class="form-outline mb-3">
                        <label class="form-label">Email <span class="text-danger">*</span></label>
                        <input type="email" name="email" class="form-control form-control-md"
                        placeholder="Email" style="border-radius:10px;" value="{{old('email')}}" />
                        @if ($errors->has('email'))
                        <span class="help-block errorText">
                        {{ $errors->first('email') }}
                        </span>
                        @endif
                      </div>
                    </div>
                    <div class="col-md-6 col-12">
                      <div class="form-outline mb-3">
                        <label class="form-label">Phone <span class="text-danger">*</span></label>
                        <input type="text" name="phone" id="phone" class="form-control contactdata form-control-md"
                        placeholder="123-456-7890" style="border-radius:10px;" maxlength="12" value="{{old('phone')}}" /> 
                      
                      @if ($errors->has('phone'))
                        <span class="help-block errorText">
                        {{ $errors->first('phone') }}
                        </span>
                      @endif
                      </div>
                    </div>
          </div>
          
			</div>
			
              <div class="col-md-6">
                <div class="row mb-4">
                  <p class="fw-bold mb-1 pb-2">Billing Address :</p>
                  <!--  <div class="col-md-6 col-12">
                    
                    <div class="form-outline mb-3">
                        <label class="form-label">First Name <span class="text-danger">*</span></label>
                        <input type="text" name="first_name" class="form-control form-control-md"
                        placeholder="First name" style="border-radius:10px;" value="{{old('first_name')}}" />
                        @if ($errors->has('first_name'))
                        <span class="help-block errorText">
                        {{ $errors->first('first_name') }}
                        </span>
                        @endif
                      </div>
                    </div>
                    <div class="col-md-6 col-12">
                      <div class="form-outline mb-3">
                        <label class="form-label">Last Name <span class="text-danger">*</span></label>
                        <input type="text" name="last_name" class="form-control form-control-md"
                        placeholder="Last name" style="border-radius:10px;" value="{{old('last_name')}}" />
                        @if ($errors->has('last_name'))
                        <span class="help-block errorText">
                        {{ $errors->first('last_name') }}
                        </span>
                        @endif
                      </div>
                    </div>
                  
                    <div class="col-md-6 col-12">
                      <div class="form-outline mb-3">
                        <label class="form-label">Email <span class="text-danger">*</span></label>
                        <input type="email" name="email" class="form-control form-control-md"
                        placeholder="Email" style="border-radius:10px;" value="{{old('email')}}" />
                        @if ($errors->has('email'))
                        <span class="help-block errorText">
                        {{ $errors->first('email') }}
                        </span>
                        @endif
                      </div>
                    </div>
                    <div class="col-md-6 col-12">
                      <div class="form-outline mb-3">
                        <label class="form-label">Phone <span class="text-danger">*</span></label>
                        <input type="text" name="phone" id="phone" class="form-control contactdata form-control-md"
                        placeholder="123-456-7890" style="border-radius:10px;" maxlength="12" value="{{old('phone')}}" /> 
                      
                      @if ($errors->has('phone'))
                        <span class="help-block errorText">
                        {{ $errors->first('phone') }}
                        </span>
                      @endif
                      </div>
                    </div>
               -->
                    <div class="col-md-12 col-12">
                      <div class="form-outline mb-3">
                        <label class="form-label">Street Address</label>
                        <input type="text" name="street_address" class="form-control form-control-md"
                        placeholder="Street Address" style="border-radius:10px;" value="{{old('street_address')}}" />
                        @if ($errors->has('street_address'))
                          <span class="help-block errorText">
                          {{ $errors->first('street_address') }}
                          </span>
                        @endif
                      </div>
                    </div>

                    <div class="col-md-5 col-12">
                      <div class="form-outline mb-3">
                        <label class="form-label">City</label>
                        <input type="text" name="city" class="form-control form-control-md"
                        placeholder="City" style="border-radius:10px;" value="{{old('city')}}" />
                        @if ($errors->has('city'))
                          <span class="help-block errorText">
                          {{ $errors->first('city') }}
                          </span>
                        @endif
                      </div>
                    </div>
                    <div class="col-md-7 col-12">
                      <div class="form-outline mb-3">
                        <label class="form-label" >Zip Code</label>
                        <input type="text" name="zip_code" class="form-control numonly form-control-md"
                        placeholder="Zip Code" style="border-radius:10px;" maxlength="5" value="{{old('zip_code')}}" />
                        @if ($errors->has('zip_code'))
                          <span class="help-block errorText">
                          {{ $errors->first('zip_code') }}
                          </span>
                        @endif
                      </div>
                    </div>


                    <div class="col-md-5 col-12">
                      <div class="form-outline mb-3">
                        <label class="form-label">Country <span class="text-danger">*</span></label>
                        <select name="country" id="country" class="form-control" style="border-radius:10px;">
                        <option value="">Select Country</option>
                        <option value="US" {{ old('country') == 'US' ? 'selected' : '' }}>USA</option>
                        <option value="CA" {{ old('country') == 'CA' ? 'selected' : '' }}>Canada</option>
                        </select>
                        @if ($errors->has('country'))
                        <span class="help-block errorText">
                        {{ $errors->first('country') }}
                        </span>
                        @endif
                      </div>                    
                    </div>
                    <div class="col-md-7 col-12">
                      <div class="form-outline mb-3">
                        <label class="form-label">State</label>
                        <select class="form-control form-control-xl" name="region" id="region" style="border-radius:10px;">
                          <option value="">Select State</option>
                          <optgroup label="USA" id="usaOptgroup">
                          <option value="AL" {{ old('region') == 'AL' ? 'selected' : '' }}>AL</option>
                          <option value="AK" {{ old('region') == 'AK' ? 'selected' : '' }}>AK</option>
                          <option value="AZ" {{ old('region') == 'AZ' ? 'selected' : '' }}>AZ</option>
                          <option value="AR" {{ old('region') == 'AR' ? 'selected' : '' }}>AR</option>
                          <option value="CA" {{ old('region') == 'CA' ? 'selected' : '' }}>CA</option>
                          <option value="CO" {{ old('region') == 'CO' ? 'selected' : '' }}>CO</option>
                          <option value="CT" {{ old('region') == 'CT' ? 'selected' : '' }}>CT</option>
                          <option value="DE" {{ old('region') == 'DE' ? 'selected' : '' }}>DE</option>
                          <option value="FL" {{ old('region') == 'FL' ? 'selected' : '' }}>FL</option>
                          <option value="GA" {{ old('region') == 'GA' ? 'selected' : '' }}>GA</option>
                          <option value="HI" {{ old('region') == 'HI' ? 'selected' : '' }}>HI</option>
                          <option value="ID" {{ old('region') == 'ID' ? 'selected' : '' }}>ID</option>
                          <option value="IL" {{ old('region') == 'IL' ? 'selected' : '' }}>IL</option>
                          <option value="IN" {{ old('region') == 'IN' ? 'selected' : '' }}>IN</option>
                          <option value="IA" {{ old('region') == 'IA' ? 'selected' : '' }}>IA</option>
                          <option value="KS" {{ old('region') == 'KS' ? 'selected' : '' }}>KS</option>
                          <option value="KY" {{ old('region') == 'KY' ? 'selected' : '' }}>KY</option>
                          <option value="LA" {{ old('region') == 'LA' ? 'selected' : '' }}>LA</option>
                          <option value="ME" {{ old('region') == 'ME' ? 'selected' : '' }}>ME</option>
                          <option value="MD" {{ old('region') == 'MD' ? 'selected' : '' }}>MD</option>
                          <option value="MA" {{ old('region') == 'MA' ? 'selected' : '' }}>MA</option>
                          <option value="MI" {{ old('region') == 'MI' ? 'selected' : '' }}>MI</option>
                          <option value="MN" {{ old('region') == 'MN' ? 'selected' : '' }}>MN</option>
                          <option value="MS" {{ old('region') == 'MS' ? 'selected' : '' }}>MS</option>
                          <option value="MO" {{ old('region') == 'MO' ? 'selected' : '' }}>MO</option>
                          <option value="MT" {{ old('region') == 'MT' ? 'selected' : '' }}>MT</option>
                          <option value="NE" {{ old('region') == 'NE' ? 'selected' : '' }}>NE</option>
                          <option value="NV" {{ old('region') == 'NV' ? 'selected' : '' }}>NV</option>
                          <option value="NH" {{ old('region') == 'NH' ? 'selected' : '' }}>NH</option>
                          <option value="NJ" {{ old('region') == 'NJ' ? 'selected' : '' }}>NJ</option>
                          <option value="NM" {{ old('region') == 'NM' ? 'selected' : '' }}>NM</option>
                          <option value="NY" {{ old('region') == 'NY' ? 'selected' : '' }}>NY</option>
                          <option value="NC" {{ old('region') == 'NC' ? 'selected' : '' }}>NC</option>
                          <option value="ND" {{ old('region') == 'ND' ? 'selected' : '' }}>ND</option>
                          <option value="OH" {{ old('region') == 'OH' ? 'selected' : '' }}>OH</option>
                          <option value="OK" {{ old('region') == 'OK' ? 'selected' : '' }}>OK</option>
                          <option value="OR" {{ old('region') == 'OR' ? 'selected' : '' }}>OR</option>
                          <option value="PA" {{ old('region') == 'PA' ? 'selected' : '' }}>PA</option>
                          <option value="RI" {{ old('region') == 'RI' ? 'selected' : '' }}>RI</option>
                          <option value="SC" {{ old('region') == 'SC' ? 'selected' : '' }}>SC</option>
                          <option value="SD" {{ old('region') == 'SD' ? 'selected' : '' }}>SD</option>
                          <option value="TN" {{ old('region') == 'TN' ? 'selected' : '' }}>TN</option>
                          <option value="TX" {{ old('region') == 'TX' ? 'selected' : '' }}>TX</option>
                          <option value="UT" {{ old('region') == 'UT' ? 'selected' : '' }}>UT</option>
                          <option value="VT" {{ old('region') == 'VT' ? 'selected' : '' }}>VT</option>
                          <option value="VA" {{ old('region') == 'VA' ? 'selected' : '' }}>VA</option>
                          <option value="WA" {{ old('region') == 'WA' ? 'selected' : '' }}>WA</option>
                          <option value="WV" {{ old('region') == 'WV' ? 'selected' : '' }}>WV</option>
                          <option value="WI" {{ old('region') == 'WI' ? 'selected' : '' }}>WI</option>
                          <option value="WY" {{ old('region') == 'WY' ? 'selected' : '' }}>WY</option>
                          </optgroup>
                          <optgroup label="Canada">
                            <option value="AB" {{ old('region') == 'AB' ? 'selected' : '' }}>Alberta</option>
                            <option value="BC" {{ old('region') == 'BC' ? 'selected' : '' }}>British Columbia</option>
                            <option value="MB" {{ old('region') == 'MB' ? 'selected' : '' }}>Manitoba</option>
                            <option value="NB" {{ old('region') == 'NB' ? 'selected' : '' }}>New Brunswick</option>
                            <option value="NL" {{ old('region') == 'NL' ? 'selected' : '' }}>Newfoundland and Labrador</option>
                            <option value="NT" {{ old('region') == 'NT' ? 'selected' : '' }}>Northwest Territories</option>
                            <option value="NS" {{ old('region') == 'NS' ? 'selected' : '' }}>Nova Scotia</option>
                            <option value="NU" {{ old('region') == 'NU' ? 'selected' : '' }}>Nunavut</option>
                            <option value="ON" {{ old('region') == 'ON' ? 'selected' : '' }}>Ontario</option>
                            <option value="PE" {{ old('region') == 'PE' ? 'selected' : '' }}>Prince Edward Island</option>
                            <option value="QC" {{ old('region') == 'QC' ? 'selected' : '' }}>Quebec</option>
                            <option value="SK" {{ old('region') == 'SK' ? 'selected' : '' }}>Saskatchewan</option>
                            <option value="YT" {{ old('region') == 'YT' ? 'selected' : '' }}>Yukon</option>                           
                          </optgroup>
                        </select>
                      </div> 
                    </div>
                    <div class="col-md-5 col-12">
                      <div class="form-outline mb-3 my-5">                        
                        <a id="continue-payment" href="{{route('home')}}" class="btn btn-success btn-md btn-block text-center" style="border-radius:10px; width:100%;">Cancel</a>
                      </div>                    
                    </div>
                    <div class="col-md-7 col-12">
                      <div class="form-outline mb-3 my-5">                       
                        <input type ="hidden" name="payment_gateway_type" value="STRIPE"/>               
                        <button class="btn btn-success btn-md btn-block text-center" style="border-radius:10px; width:100%;" id="submit">Pay Now</button>                                           
                      </div> 
                    </div>
				        </div>
                
			        </div>
			
			      <!--
			        <div class="row col-12">
                <div class="col-md-6 col-6 mt-5 mb-0">  
                  <input type ="hidden" name="payment_gateway_type" value="STRIPE"/>               
                  <button class="btn btn-success btn-md btn-block text-center" style="border-radius:10px; width:100%;" id="submit">Pay Now</button>                    
                </div>
                <div class="col-md-6 col-6 mt-5 mb-0">                    
                    <a id="continue-payment" href="{{route('home')}}" class="btn btn-success btn-md btn-block text-center" style="border-radius:10px; width:100%;">Cancel</a>
                </div>
                
              </div>
    -->
              <!-- <div id="error-message" class="error"></div> -->
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</section>
<script>
    // $(function() {      
    //     $( "form" ).submit(function(e) {
    //       $('#btnPayNow').prop('disabled');
    //         $('#loader').show();

    //     });
    // });
</script>
<style>
  .error{
    color:#cd1000;
    font-size: 17px;
  }
</style>
<script src="{{asset('js/jquery-ui.min.js')}}"></script>
<script src="{{asset('js/validate.min.js')}}"></script>
<script src="https://js.stripe.com/v3/"></script>
<script type="text/javascript">
        $(document).ready(function() {

          // Initially hide state select
      // Listen for the popstate event (back or forward navigation)
      window.addEventListener('popstate', function () {
                  // Deactivate the pre-loader when navigating back
                  $('#loader').hide();
              });
      $('#region').prop('selectedIndex', 0);
      $('#region optgroup[label="Canada"]').hide();
      $('#region optgroup[label="USA"]').hide();
      // On change of country select
      $('#country').change(function () {
        // Hide all region options
       // $('.region-select').hide();
        
        // Get the selected country value
        var selectedCountry = $(this).val();

        // Show the region options for the selected country
        if (selectedCountry === 'US') {        
          $('#region').prop('selectedIndex', 0);
          $('#region optgroup[label="USA"]').show();
          $('#region optgroup[label="Canada"]').hide();
        } else if (selectedCountry === 'CA') {        
          $('#region').prop('selectedIndex', 0);
          $('#region optgroup[label="Canada"]').show();
          $('#region optgroup[label="USA"]').hide();
        }
      });

      $('#phone').on('input', function () {
        // Remove non-numeric characters from the input
        let phone = $(this).val().replace(/\D/g, '');

        // Apply formatting as the user types
        if (phone.length > 3 && phone.length <= 6) {
            phone = phone.replace(/(\d{3})(\d+)/, '$1-$2');
        } else if (phone.length > 6) {
            phone = phone.replace(/(\d{3})(\d{3})(\d+)/, '$1-$2-$3');
        }

        // Update the input field with the formatted phone number
        $(this).val(phone);
    });

            // $("#paymentform").validate({
            //   rules: {
            //       order_amount: {
            //             required: true,
            //             number:true
            //       },
            //       card_holder_name: {
            //             required: true
            //       },
            //       card_number: {
            //             required: true
            //       },
            //       expiry: {
            //             required: true
            //       },
            //       cvv: {
            //             required: true,
            //             number:true,
            //             maxlength:3,
            //             minlength:3
            //       },
            //       first_name: {
            //             required: true
            //       },
            //       last_name: {
            //             required: true
            //       },
            //       email: {
            //             required: true,
            //             email:true
            //       },
            //       phone: {
            //             required: true
            //       },
            //       country:{
            //         required:true
            //       }
            //     },
            //   messages: {
            //       order_amount: {
            //             required: "Enter Pay Amount",
            //             number: "Enter Number Only"
            //       },
            //       card_holder_name: {
            //             required: "Enter Card holder name"
            //       },
            //       card_number: {
            //             required: "Enter Card Number"
            //       },
            //       expiry: {
            //             required: "Enter Card Expiry Date"
            //       },
            //       cvv: {
            //             required: "Enter CVV Number",
            //             number: "Enter Number Only"
            //       },
            //       first_name: {
            //             required: "Enter First name"
            //       },
            //       last_name: {
            //             required: "Enter Last name"
            //       },
            //       email: {
            //             required: "Enter Email Id",
            //             email: "Enter Valid Email Id"
            //       },
            //       phone: {
            //             required: "Enter Phone Number"
            //       },
            //       country: {
            //             required: "Select Country"
            //       },
            //     },

            // });
        });
    </script>
<script>
$(document).ready(function() {
  $(".card_data").on("input", function() {
    // Remove non-digit characters
    var cardNumber = $(this).val().replace(/\D/g, "");

    // Add a space every 4 digits
    var formattedCardNumber = "";
    for (var i = 0; i < cardNumber.length; i++) {
      if (i > 0 && i % 4 === 0) {
        formattedCardNumber += " ";
      }
      formattedCardNumber += cardNumber.charAt(i);
    }

    // Update the input value with the formatted card number
    $(this).val(formattedCardNumber);
  });
});
</script>
<script>
    function formatString(e) {
  var inputChar = String.fromCharCode(event.keyCode);
  var code = event.keyCode;
  var allowedKeys = [8];
  if (allowedKeys.indexOf(code) !== -1) {
    return;
  }

  event.target.value = event.target.value.replace(
    /^([1-9]\/|[2-9])$/g, '0$1/' // 3 > 03/
  ).replace(
    /^(0[1-9]|1[0-2])$/g, '$1/' // 11 > 11/
  ).replace(
    /^([0-1])([3-9])$/g, '0$1/$2' // 13 > 01/3
  ).replace(
    /^(0?[1-9]|1[0-2])([0-9]{2})$/g, '$1/$2' // 141 > 01/41
  ).replace(
    /^([0]+)\/|[0]+$/g, '0' // 0/ > 0 and 00 > 0
  ).replace(
    /[^\d\/]|^[\/]*$/g, '' // To allow only digits and `/`
  ).replace(
    /\/\//g, '/' // Prevent entering more than 1 `/`
  );
}

</script>
<script>
    $(".pricess").blur(function(e){
    e.preventDefault();
    var val = parseFloat($(this).val());
    if(val){
        $(this).val(val.toFixed(2));
    }
    else
    {
        $(this).val();
    }
    });
</script>
<script type="text/javascript">
    $('.numonly').keypress(function(event) {
      if (event.which != 46 && (event.which < 47 || event.which > 59))
      {
          event.preventDefault();
          if ((event.which == 46) && ($(this).indexOf('.') != -1)) {
              event.preventDefault();
          }
      }
    });
</script>

<script>
    $(document).ready(function() {
      $('.contactdata').keypress(function(event) {
          var charCode = event.which || event.keyCode;
          if (charCode > 31 && (charCode < 48 || charCode > 57) && charCode != 45) {
          return false;
          } else {
          var len = $(this).val().length;
          var index = $(this).val().indexOf('-');
          if (index != -1 && charCode == 45) {
              return false;
          }
          if (len == 3 || len == 7) {
              $(this).val($(this).val() + '-');
          }
          }
      });
      });
  </script>
  <script>
       const stripe = Stripe("{{ env('STRIPE_PUBLISH_KEY') }}"); // Replace with your publishable key
       //const stripe = Stripe("pk_test_51QAZl1Eosef2JpoeHIXJH0fY93iuRPLAACjPA0npW77tONbwGIYVIj2RdTHmJjx4fAAqxere3kM8OQgbN7Crc58f00vrL1Mpn2"); // Replace with your publishable key
        const elements = stripe.elements();
       // const cardElement = elements.create('card');
        const cardElement = elements.create('card', {
          hidePostalCode: true,  
          style: {
              base: {
                  color: '#959895',
                  fontFamily: '"Helvetica Neue", Helvetica, sans-serif',
                  fontSmoothing: 'antialiased',
                  fontSize: '16px',
                  '::placeholder': {
                      color: '#959895'
                  }                 
              },
              invalid: {
                  color: '#fa755a',
                  iconColor: '#fa755a'
              }
          }
      });
        cardElement.mount('#card-element');
		
		// Handle real-time validation errors from the card Element
		cardElement.on('change', function(event) {
			var displayError = document.getElementById('error-message');
			if (event.error) {
				displayError.textContent = event.error.message;
			} else {
				displayError.textContent = '';
			}
		});
		
        const form = document.getElementById('paymentform');
        form.addEventListener('submit', async (event) => {
            event.preventDefault();
            /* validation code starts */
              document.getElementById('error-message').innerText = '';
              const orderAmount = form.order_amount.value;             
              const cardHolderName = form.card_holder_name.value.trim();
             // const cardnumber = form.cardnumber.value.trim();
              const firstName = form.first_name.value.trim();
              const lastName = form.last_name.value.trim();
              const email = form.email.value.trim();
              const phone = form.phone.value.trim();
              const streetAddress = form.street_address.value.trim();
              const city = form.city.value.trim();
              const region = form.region.value.trim();
              const zipCode = form.zip_code.value.trim();
              const country = form.country.value.trim();
              if (!orderAmount || isNaN(orderAmount) || Number(orderAmount) <= 0) {
                  document.getElementById('error-message').innerText = 'Your order amount is incomplete.';
                  return; 
              }
              if (!cardHolderName) {
                  document.getElementById('error-message').innerText = 'Your card holder name is incomplete.';
                  return;
              }
              
              // if (!cardnumber) {
              //     document.getElementById('error-message').innerText = 'Your card holder name is incomplete.';
              //     return;
              // }
              
              if (!firstName) {
                  document.getElementById('error-message').innerText = 'Your first name is incomplete.';
                  return;
              }
              
              if (!lastName) {
                  document.getElementById('error-message').innerText = 'Your last name is incomplete.';
                  return;
              }
              
              const emailRegex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
              if (!email || !emailRegex.test(email)) {
                  document.getElementById('error-message').innerText = 'Your email address is invalid.';
                  return;
              }

              const phoneRegex = /^[0-9]{3}-[0-9]{3}-[0-9]{4}$/;
              if (!phone || !phoneRegex.test(phone)) {
                  document.getElementById('error-message').innerText = 'Your phone number is invalid. It must be in the format 123-456-7890.';
                  return;
              }
              
              if (!streetAddress) {
                  document.getElementById('error-message').innerText = 'Your street address is incomplete.';
                  return;
              }
              
              if (!city) {
                  document.getElementById('error-message').innerText = 'Your city is incomplete.';
                  return;
              }

              const zipCodeRegex = /^[0-9]{5,10}$/;
              if (!zipCode || !zipCodeRegex.test(zipCode)) {
                  document.getElementById('error-message').innerText = 'Your zip code is invalid.';
                  return;
              }
              
              if (!country) {
                  document.getElementById('error-message').innerText = 'Your country is incomplete.';
                  return;
              }
            
              if (!region) {
                  document.getElementById('error-message').innerText = 'Your region/state is incomplete.';
                  return;
              }
            /* validation code ends */
            document.getElementById('error-message').innerText = '';

            const { error, paymentMethod } = await stripe.createPaymentMethod({
                type: 'card',
                card: cardElement,
                billing_details: {
                    address: {
                        line1: form.street_address.value,
                        city: form.city.value,
                        state: form.region.value,
                        postal_code: form.zip_code.value,
                        country: form.country.value
                    }
                }
            });

            if (error) {
                document.getElementById('error-message').innerText = error.message;
            } else {
                // Send paymentMethod.id and billing details to your server
                fetch('{{route("add-customer-payment")}}', {
                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/json',
                    },
                    body: JSON.stringify({
                        payment_method_id: paymentMethod.id,                        
                        order_amount: form.order_amount.value,
                        card_holder_name: form.card_holder_name.value,
                        first_name: form.first_name.value,
                        last_name: form.last_name.value,
                        email: form.email.value,
                        phone: form.phone.value,
                        street_address: form.street_address.value,
                        city: form.city.value,
                        zip_code: form.zip_code.value,
                        country: form.country.value,
                        region: form.region.value,
                        payment_gateway_type:'STRIPE',
                        _token: '{{ csrf_token() }}'
                    }),
                }).then((response) => response.json())
                .then((data) => {
                    $('#loader').hide();
                    $('#btnPayNow').prop('disabled', false);
                    if (data.error) {
                        document.getElementById('error-message').innerText = data.error;
                    } else {
                        alert('Payment successful! Charge ID: ' + data.charge_id);
                    }
                });
            }
        });
    </script>
</body>
</html>
