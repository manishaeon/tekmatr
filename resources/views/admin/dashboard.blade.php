@extends('includes.base')
<style>

</style>
@section('content')
<div class="content-inner mt-5 py-0">
   <div class="row g-6 mb-6">
        <div class="col-xl-3 col-sm-6 col-12">
            <div class="card shadow border-0" style="background-color:#0a436d;">
                <div class="card-body text-white">
                    <div class="row">
                        <div class="col">
                            <span class="h6 font-semibold  text-sm d-block mb-2" style="color: #ffffff">Successful Payments</span>
                            <span class="h3 font-bold mb-0" style="color: #ffffff">${{number_format($todayPayment,2)}}</span>
                        </div>
                        <div class="col-auto">
                            <div class="icon icon-shape bg-tertiary text-white text-lg rounded-circle">
                                <i class="bi bi-credit-card"></i>
                            </div>
                        </div>
                    </div>
                    <div class="mt-2 mb-0 text-sm">

                        <span class="text-nowrap text-xs " style="color: #ffffff">Today</span>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xl-3 col-sm-6 col-12">
            <div class="card shadow border-0" style="background-color:#b4410f;">
                <div class="card-body text-white">
                    <div class="row">
                        <div class="col">
                            <span class="h6 font-semibold  text-sm d-block mb-2" style="color: #ffffff">Successful Payments</span>
                            <span class="h3 font-bold mb-0" style="color: #ffffff">${{number_format($currentWeek,2)}}</span>
                        </div>
                        <div class="col-auto">
                            <div class="icon icon-shape bg-tertiary text-white text-lg rounded-circle">
                                <i class="bi bi-credit-card"></i>
                            </div>
                        </div>
                    </div>
                    <div class="mt-2 mb-0 text-sm">

                        <span class="text-nowrap text-xs " style="color: #ffffff">Current Week</span>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xl-3 col-sm-6 col-12">
            <div class="card shadow border-0" style="background-color: #ffc60d;">
                <div class="card-body text-white">
                    <div class="row">
                        <div class="col">
                            <span class="h6 font-semibold  text-sm d-block mb-2" style="color: #ffffff">Successful Payments</span>
                            <span class="h3 font-bold mb-0" style="color: #ffffff">${{number_format($currentMonthTotalAmount,2)}}</span>
                        </div>
                        <div class="col-auto">
                            <div class="icon icon-shape bg-tertiary text-white text-lg rounded-circle">
                                <i class="bi bi-credit-card"></i>
                            </div>
                        </div>
                    </div>
                    <div class="mt-2 mb-0 text-sm">

                        <span class="text-nowrap text-xs " style="color: #ffffff">Current Month</span>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xl-3 col-sm-6 col-12">
            <div class="card shadow border-0" style="background-color: #c41010;">
                <div class="card-body text-white">
                    <div class="row">
                        <div class="col">
                            <span class="h6 font-semibold  text-sm d-block mb-2" style="color: #ffffff">Successful Payments</span>
                            <span class="h3 font-bold mb-0" style="color: #ffffff">${{number_format($totalAmount,2)}}</span>
                        </div>
                        <div class="col-auto">
                            <div class="icon icon-shape bg-tertiary text-white text-lg rounded-circle">
                                <i class="bi bi-credit-card"></i>
                            </div>
                        </div>
                    </div>
                    <div class="mt-2 mb-0 text-sm">

                        <span class="text-nowrap text-xs " style="color: #ffffff">All Payments</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
