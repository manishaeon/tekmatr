@extends('includes.base')
@section('content')
    <div class="content-inner mt-5 py-0">
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-header d-flex justify-content-between pb-0 border-0">
                        <div class="header-title d-flex align-items-center justify-content-between w-100">
                            <h4 class="card-title">Payment List</h4>
                            {{-- <a href="{{route('new-driver')}}" class="btn btn-primary">Add New Driver</a> --}}
                        </div>
                    </div>
                    <div class="card-body">

                        <div class="table-responsive">
                            <table id="datatable" class="table table-striped text-center" data-toggle="data-table">
                                <thead>
                                    <tr>
                                        <th>No.</th>
                                        <th>Date</th>
                                        <th>Name</th>
                                        <th>Amount</th>
                                        <th>City</th>
                                        <th>Zip</th>
                                        {{-- <th>Transfer ID</th> --}}
                                        <th>Status</th>
                                        <th>Batch Status</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @php $i =1 ; @endphp
                                    @foreach($payment as $rows)
                                    <tr>
                                        <td>{{$i++;}}.</td>
                                        <td>{{ date('M d',strtotime($rows->created_at)) }}</td>
                                        <td>{{$rows->name}}</td>
                                        <td>${{number_format($rows->amount,2)}}</td>
                                        <td>{{$rows->city}}</td>
                                        <td>{{$rows->pin_code}}</td>
                                        {{-- <td>{{$rows->transcation_id}}</td> --}}
                                        <td id="status-{{$rows->id}}">
                                            @if($rows->payment_status=="paid")
                                            <span class="badge bg-primary">SUCCEEDED</span>
                                            @elseif($rows->payment_status=="pending")
                                            <span class="badge bg-warning">PENDING</span>                                           
                                            @elseif($rows->payment_status=="refunded")
                                            <span class="badge bg-warning">REFUNDED</span>
                                            @else
                                            <span class="badge bg-danger">FAILED</span>
                                            @endif
                                        </td>
                                        {{--
                                        @if($rows->payment_status=="paid")
                                            <td>
                                                @if($rows->batch_status=="paid")
                                                <button class="btn btn-danger btn-sm" id="{{$rows->id}}" onclick="changeBatchStatus(this.id,'pending');">Pending</button>
                                                @else
                                                <button class="btn btn-success btn-sm" id="{{$rows->id}}" onclick="changeBatchStatus(this.id,'paid');">Paid</button>
                                                @endif
                                            </td>
                                        @else
                                        <td>
                                            --
                                        </td>
                                        @endif
                                        --}}
                                       
                                        @if($rows->payment_status=="paid" || $rows->payment_status=="refunded")
                                            <td>
                                                <!-- <select class="form-control form-control-xl btn btn-success btn-sm" name="batch" id="{{$rows->id}}" onchange="changeBatchStatus(this.id,'paid');" style="border-radius:10px;">
                                                    <option value="paid" class="btn btn-success btn-sm" id="{{$rows->id}}"  {{ $rows->payment_status == 'paid' ? 'selected' : '' }}>Paid</option>
                                                    <option value="pending" class="btn btn-danger btn-sm" id="{{$rows->id}}" {{ $rows->payment_status == 'pending' ? 'selected' : '' }}>Pending</option>
                                                    <option value="refund" class="btn btn-warning btn-sm" id="{{$rows->id}}" {{ $rows->payment_status == 'refund' ? 'selected' : '' }}>Refund</option>
                                                </select> -->
                                                    @php
                                                        $dropdown_menu="";
                                                    @endphp
                                                    @if($rows->batch_status=="paid")
                                                        @php 
                                                            $status ="Paid";
                                                            $class="btn-success";
                                                            $dropdown_menu=" ";
                                                        @endphp
                                                    @elseif($rows->batch_status=="pending")
                                                        @php 
                                                            $status ="pending";
                                                            $class="btn-danger";
                                                            $dropdown_menu="dropdown-menu";
                                                        @endphp
                                                    @else
                                                        @php 
                                                            $status ="Refund";
                                                            $class="btn-warning";
                                                            $dropdown_menu=" ";
                                                        @endphp
                                                    @endif
                                                <div class="dropdown">
                                                @if($rows->batch_status=="paid")  
                                                    <button class="btn {{$class}} btn-sm" type="button"  aria-expanded="false" style="cursor: default;">Paid</button>
                                                @endif   
                                                @if($rows->batch_status=="pending")    
                                                        <button class="btn {{$class}} dropdown-toggle btn-sm" data-bs-theme="dark" type="button" data-bs-toggle="dropdown" aria-expanded="false" id="pnd-{{$rows->id}}">
                                                        {{$status}}
                                                        </button>                                                       
                                                            <ul class="{{$dropdown_menu}}" aria-labelledby="dropdownMenuButton1" >   
                                                                <li><a class="dropdown-item"  id="{{$rows->id}}" onclick="changeBatchStatus(this.id,'paid');" style="cursor: pointer;">Paid</a></li>
                                                                <li><a class="dropdown-item" id="{{$rows->id}}" onclick="changeBatchStatus(this.id,'refund');" style="cursor: pointer;">Refund</a></li>                                                   
                                                            </ul>                                                        
                                                @endif
                                                @if($rows->batch_status=="refund")  
                                                    <button class="btn {{$class}} btn-sm" type="button"  aria-expanded="false" style="cursor: default;">Refund</button>
                                                @endif                                                
                                                </div>
                                                <div class="button-container-{{$rows->id}}">
                                                </div>
                                            </td>
                                        @else
                                        <td>
                                            --
                                        </td>
                                        @endif
                                        
                                        <td>
                                            @php
                                            $id = base64_encode($rows->id);
                                            @endphp
                                            <a href="{{ url('invoice/' . $id.'/0') }}" target="_BLANK" class="btn btn-info btn-sm"><i class="fa fa-print"></i> Invoice</a>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
    <script> 
        function changeBatchStatus(id,status)
        {            
           
            var datatable = $('#datatable').DataTable();
            var info = datatable.page.info();            
            var id = id;
            var status = status;
            var currentPage =  info.page + 1;
            var statusTD = document.getElementById('status-'+id);
          
            
            if(confirm("Are you Sure  to change the Batch Status ?")){
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                // var url = 'changeVariationCategory';
                $.ajax({
                    url:"{{ url('change-payment-status') }}",
                    method:"POST",
                    data:{id:id,status:status},
                    success:function(data)
                    {                      
                       if (status === 'refund') {                       
                            // Show the button dynamically
                            var refundButton = '<button class="btn btn-warning btn-sm" type="button" aria-expanded="false" style="cursor: default;">Refund</button>';
                            // Assuming you want to append the button to a specific element with class 'button-container'
                            $('.button-container-'+id).append(refundButton);
                            statusTD.innerHTML = '<span class="badge bg-warning">REFUNDED</span>';
                            $('#pnd-'+id).hide();
                        }

                        if (status === 'paid') {                       
                            // Show the button dynamically
                            var refundButton = '<button class="btn btn-success btn-sm" type="button" aria-expanded="false" style="cursor: default;">Paid</button>';
                            // Assuming you want to append the button to a specific element with class 'button-container'
                            $('.button-container-'+id).append(refundButton);
                            $('#pnd-'+id).hide();
                        }
                        // $('.table').load(window.location + ' .table');
                       // datatable.clear().rows.add(data.newData).draw();
                         datatable.page(currentPage - 1).draw('page');  
                       // datatable.ajax.reload();                      
                    }
            });
           }
        }
    </script>
@endsection
