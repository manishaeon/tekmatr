<!doctype html>
<html lang="en" dir="ltr">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>TekMatr Admin Login</title>

    <!-- Favicon -->
    <link rel="shortcut icon" href="{{ asset('images/tek-logo.png') }}" />

    <!-- Library / Plugin Css Build -->
    <link rel="stylesheet" href="{{ asset('css/core/libs.min.css') }}">

    <!-- Custom Css -->
    <link rel="stylesheet" href="{{ asset('css/aprycot.css?v=1.0.0') }}">
    <style>
        .divider:after,
        .divider:before {
            content: "";
            flex: 1;
            height: 1px;
            background: #eee;
        }
        .h-custom {
           height: calc(100% - 73px);
        }
        @media (max-width: 450px) {
            .h-custom {
               height: 100%;
            }
        }

    </style>
</head>

<body class=" " data-bs-spy="scroll" data-bs-target="#elements-section" data-bs-offset="0" tabindex="0">

    <div class="wrapper">
        <section class="vh-100">
            <div class="container-fluid h-custom">
              <div class="row d-flex justify-content-center align-items-center h-100">
                <div class="col-md-9 col-lg-6 col-xl-5">
                  <img src="https://mdbcdn.b-cdn.net/img/Photos/new-templates/bootstrap-login-form/draw2.webp"
                    class="img-fluid" alt="Sample image">
                </div>
                <div class="col-md-8 col-lg-6 col-xl-4 offset-xl-1">
                  <form method="POST" action="{{ url('login') }}">
                    @csrf
                    <div class="divider d-flex align-items-center my-4">
                      <img src="{{asset('images/tek-logo.png')}}" alt="">
                    </div>
                    <div class="row">
                        <div class="col-lg-12">
                         @if ($errors->any())
                         <div class="d-flex justify-content-center">
                            <ul class="list-group text-danger">
                                @foreach ($errors->all() as $error)
                                    <li>
                                        {{ $error }}
                                    </li>
                                @endforeach
                            </ul>
                         </div>
                         @endif
                     </div>
                    </div>
                    <!-- Email input -->
                    <div class="form-outline mb-4">
                      <label class="form-label" for="form3Example3">Email address</label>
                      <input type="email" name="email" id="form3Example3" class="form-control form-control-md"
                        placeholder="Enter a valid email address" />

                    </div>

                    <!-- Password input -->
                    <div class="form-outline mb-3">
                      <label class="form-label" for="form3Example4">Password</label>
                      <input type="password" name="password" id="form3Example4" class="form-control form-control-md"
                        placeholder="Enter password" />

                    </div>


                    <div class="text-center text-lg-start mt-4 pt-2">
                      <button type="submit" class="btn btn-primary btn-lg"
                        style="padding-left: 2.5rem; padding-right: 2.5rem;">Login</button>
                    </div>

                  </form>
                </div>
              </div>
            </div>
          </section>

    </div>

    <!-- Required Library Bundle Script -->
    <script src="{{ asset('js/core/libs.min.js') }}"></script>

    <!-- External Library Bundle Script -->
    <script src="{{ asset('js/core/external.min.js') }}"></script>

    <!-- Widgetchart JavaScript -->
    <script src="{{ asset('js/charts/widgetcharts.js') }}"></script>

    <!-- Mapchart JavaScript -->
    <script src="{{ asset('js/charts/vectore-chart.js') }}"></script>
    <script src="{{ asset('js/charts/dashboard.js') }}"></script>

    <!-- Admin Dashboard Chart -->
    <script src="{{ asset('js/charts/admin.js') }}"></script>

    <!-- fslightbox JavaScript -->
    <script src="{{ asset('js/fslightbox.js') }}"></script>

    <!-- GSAP Animation -->
    <script src="{{ asset('vendor/gsap/gsap.min.js') }}"></script>
    <script src="{{ asset('vendor/gsap/ScrollTrigger.min.js') }}"></script>
    <script src="{{ asset('js/animation/gsap-init.js') }}"></script>

    <!-- Stepper Plugin -->
    <script src="{{ asset('js/stepper.js') }}"></script>

    <!-- Form Wizard Script -->
    <script src="{{ asset('js/form-wizard.js') }}"></script>

    <!-- app JavaScript -->
    <script src="{{ asset('js/app.js') }}"></script>

    <!-- moment JavaScript -->
    <script src="{{ asset('vendor/moment.min.js') }}"></script>
</body>

</html>
