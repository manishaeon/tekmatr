<!doctype html>
<html lang="en" dir="ltr">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>tek matr</title>
    <meta name="csrf-token" content="{{ csrf_token() }}" />


    <link rel="shortcut icon" href="{{ asset('images/tek-logo.png') }}" />

    <!-- Library / Plugin Css Build -->
    <link rel="stylesheet" href="{{ asset('css/core/libs.min.css') }}">

    <!-- Custom Css -->
    <link rel="stylesheet" href="{{ asset('css/aprycot.css') }}">
    <link rel="stylesheet" href="{{ asset('css/jquery-ui.min.css') }}">

    <script src="https://code.jquery.com/jquery-3.6.1.min.js"
        integrity="sha256-o88AwQnZB+VDvE9tvIXrMQaPlFFSUTR+nldQm1LuPXQ=" crossorigin="anonymous"></script>


    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.2.0/css/all.min.css"
        integrity="sha512-xh6O/CkQoPOWDdYTDqeRdPCVd1SpvCA9XXcUnZS2FmJNp1coAFzvtCN9BmamE+4aHK8yyUHUSCcJHgXloTyT2A=="
        crossorigin="anonymous" referrerpolicy="no-referrer" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.2.0/js/all.min.js"
        integrity="sha512-naukR7I+Nk6gp7p5TMA4ycgfxaZBJ7MO5iC3Fp6ySQyKFHOGfpkSZkYVWV5R7u7cfAicxanwYQ5D1e17EfJcMA=="
        crossorigin="anonymous" referrerpolicy="no-referrer"></script>

    {{-- Sweet Alert --}}
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <div id='loader'></div>
    <style>
        #loader {
        display: none;
        position: fixed;
        top: 0;
        left: 0;
        right: 0;
        bottom: 0;
        width: 100%;
        background:url({{ asset('img/loader.gif') }}) no-repeat center center rgba(0,0,0,0.50);
        z-index: 99999;
    }
        .errorText{
            color: #cd1000;
            font-size: 12px;
        }
        .wp-width:{
            width: 25px;
        }
        @media screen and (min-device-width: 481px) and (max-device-width: 768px)
        {
            .wp-width{
                Width: 100%;
            }
        }
        .input-containersss {
            position: relative;
            }
            .currency-symbol {
            position: absolute;
            top: 54%;
            left: 7px;
            }
    </style>
</head>
<body style="background-color: #e5e5e5;">
    <section class="p-4 p-md-12" style="">
  <div class="row d-flex justify-content-center">
    <div class="col-md-12 col-lg-12 col-xl-6">
      <div class="card rounded-3" style="background: rgb(255 255 255);">
        <div class="card-body p-4">
          <div class="text-center mb-4">
            <img src="{{asset('images/tek-logo.png')}}" alt="" style="height: 70px;">
          </div>
          <form action="{{route('check-payment-method')}}" method="post" id="paymentform">
            @csrf
            <div class="row">
            <div class="col-lg-12">
                @if (session('message'))
                    <div class="alert alert-success">
                        {{ session('message') }}
                    </div>
                @endif
                @if (session('error'))
                    <div class="alert alert-danger">
                        {{ session('error') }}
                    </div>
                @endif
            </div>
            <div class="col-md-12">
                <p class="fw-bold mb-1 pb-2">Select Payment Option :</p>
                <div class="d-flex flex-row pb-3">
                    <div class="d-flex align-items-center pe-2">
                      <input class="form-check-input" type="radio" name="payment_option" id="card_payment"
                        value="card_payment" aria-label="..." checked />
                    </div>
                    <div class="rounded border d-flex w-100 p-3 align-items-center">
                      <label for="card_payment"><p class="mb-0">
                        <i class="fa fa-credit-card fa-lg text-primary pe-2"></i>Debit/Credit Card Payment
                      </p></label>
                    </div>
                  </div>

                  <div class="d-flex flex-row">
                    <div class="d-flex align-items-center pe-2">
                      <input class="form-check-input" type="radio" name="payment_option" id="ach_payment"
                        value="ach_payment" aria-label="..." />
                    </div>
                    <div class="rounded border d-flex w-100 p-3 align-items-center">
                        <label for="ach_payment"><p class="mb-0">
                        <i class="fa fa-credit-card fa-lg text-dark pe-2"></i>ACH/e-Check Payment
                      </p></label>

                    </div>
                  </div>
            </div>
            <input type="submit" value="Proceed to payment" class="btn btn-primary btn-block btn-lg mb-2 mt-4" />
          </form>
        </div>
      </div>
    </div>
  </div>
</section>
<style>
  .error{
    color:#cd1000;
    font-size: 12px;
  }
</style>
<script>
    // $(function() {
    //     $( "form" ).submit(function() {
    //        $('#loader').show();
    //     });
    // });
</script>
<script src="{{asset('js/jquery-ui.min.js')}}"></script>
<script src="{{asset('js/validate.min.js')}}"></script>
<script type="text/javascript">
        $(document).ready(function() {
            $("#paymentform").validate({
              rules: {
                  order_amount: {
                        required: true,
                        number:true
                  },
                  card_holder_name: {
                        required: true
                  },
                  card_number: {
                        required: true
                  },
                  expiry: {
                        required: true
                  },
                  cvv: {
                        required: true,
                        number:true,
                        maxlength:3,
                        minlength:3
                  },
                  first_name: {
                        required: true
                  },
                  last_name: {
                        required: true
                  },
                  email: {
                        required: true,
                        email:true
                  },
                  phone: {
                        required: true
                  },
                },
              messages: {
                  order_amount: {
                        required: "Enter Pay Amount",
                        number: "Enter Number Only"
                  },
                  card_holder_name: {
                        required: "Enter Card holder name"
                  },
                  card_number: {
                        required: "Enter Card Number"
                  },
                  expiry: {
                        required: "Enter Card Expiry Date"
                  },
                  cvv: {
                        required: "Enter CVV Number",
                        number: "Enter Number Only"
                  },
                  first_name: {
                        required: "Enter First name"
                  },
                  last_name: {
                        required: "Enter Last name"
                  },
                  email: {
                        required: "Enter Email Id",
                        email: "Enter Valid Email Id"
                  },
                  phone: {
                        required: "Enter Phone Number"
                  },
                },

            });
        });
    </script>
<script>
$(document).ready(function() {
  $(".card_data").on("input", function() {
    // Remove non-digit characters
    var cardNumber = $(this).val().replace(/\D/g, "");

    // Add a space every 4 digits
    var formattedCardNumber = "";
    for (var i = 0; i < cardNumber.length; i++) {
      if (i > 0 && i % 4 === 0) {
        formattedCardNumber += " ";
      }
      formattedCardNumber += cardNumber.charAt(i);
    }

    // Update the input value with the formatted card number
    $(this).val(formattedCardNumber);
  });
});
</script>
<script>
    function formatString(e) {
  var inputChar = String.fromCharCode(event.keyCode);
  var code = event.keyCode;
  var allowedKeys = [8];
  if (allowedKeys.indexOf(code) !== -1) {
    return;
  }

  event.target.value = event.target.value.replace(
    /^([1-9]\/|[2-9])$/g, '0$1/' // 3 > 03/
  ).replace(
    /^(0[1-9]|1[0-2])$/g, '$1/' // 11 > 11/
  ).replace(
    /^([0-1])([3-9])$/g, '0$1/$2' // 13 > 01/3
  ).replace(
    /^(0?[1-9]|1[0-2])([0-9]{2})$/g, '$1/$2' // 141 > 01/41
  ).replace(
    /^([0]+)\/|[0]+$/g, '0' // 0/ > 0 and 00 > 0
  ).replace(
    /[^\d\/]|^[\/]*$/g, '' // To allow only digits and `/`
  ).replace(
    /\/\//g, '/' // Prevent entering more than 1 `/`
  );
}

</script>
<script>
    $(".pricess").blur(function(e){
    e.preventDefault();
    var val = parseFloat($(this).val());
    if(val){
        $(this).val(val.toFixed(2));
    }
    else
    {
        $(this).val();
    }
    });
</script>
<script type="text/javascript">
    $('.numonly').keypress(function(event) {
      if (event.which != 46 && (event.which < 47 || event.which > 59))
      {
          event.preventDefault();
          if ((event.which == 46) && ($(this).indexOf('.') != -1)) {
              event.preventDefault();
          }
      }
    });
</script>

<script>
    $(document).ready(function() {
      $('.contactdata').keypress(function(event) {
          var charCode = event.which || event.keyCode;
          if (charCode > 31 && (charCode < 48 || charCode > 57) && charCode != 45) {
          return false;
          } else {
          var len = $(this).val().length;
          var index = $(this).val().indexOf('-');
          if (index != -1 && charCode == 45) {
              return false;
          }
          if (len == 3 || len == 7) {
              $(this).val($(this).val() + '-');
          }
          }
      });
      });
  </script>
</body>
</html>
