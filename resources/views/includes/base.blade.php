@include('includes.header')
@include('includes.sidebar')
{{-- <div class="content-inner mt-5 py-0"> --}}
    <main class="main-content">
        <div class="position-relative">
            @yield('content')
        </div>
    {{-- </main> --}}
{{-- </div> --}}
@include('includes.footer')