<!doctype html>
<html lang="en" dir="ltr">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>tek matr</title>
    <meta name="csrf-token" content="{{ csrf_token() }}">


    <link rel="shortcut icon" href="{{ asset('images/tek-logo.png') }}" />

    <!-- Library / Plugin Css Build -->
    <link rel="stylesheet" href="{{ asset('css/core/libs.min.css') }}">

    <!-- Custom Css -->
    <link rel="stylesheet" href="{{ asset('css/aprycot.css') }}">
    <link rel="stylesheet" href="{{ asset('css/jquery-ui.min.css') }}">

    <script src="https://code.jquery.com/jquery-3.6.1.min.js"
        integrity="sha256-o88AwQnZB+VDvE9tvIXrMQaPlFFSUTR+nldQm1LuPXQ=" crossorigin="anonymous"></script>


    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.2.0/css/all.min.css"
        integrity="sha512-xh6O/CkQoPOWDdYTDqeRdPCVd1SpvCA9XXcUnZS2FmJNp1coAFzvtCN9BmamE+4aHK8yyUHUSCcJHgXloTyT2A=="
        crossorigin="anonymous" referrerpolicy="no-referrer" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.2.0/js/all.min.js"
        integrity="sha512-naukR7I+Nk6gp7p5TMA4ycgfxaZBJ7MO5iC3Fp6ySQyKFHOGfpkSZkYVWV5R7u7cfAicxanwYQ5D1e17EfJcMA=="
        crossorigin="anonymous" referrerpolicy="no-referrer"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/2.4.0/jspdf.umd.min.js"></script>
    {{-- Sweet Alert --}}
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <link href="https://fonts.googleapis.com/css2?family=Cedarville+Cursive&family=Dancing+Script&family=Lato&family=Poppins:wght@300;400&display=swap" rel="stylesheet">
    <div id='loader'></div>
    <style>
    #loader {
        display: none;
        position: fixed;
        top: 0;
        left: 0;
        right: 0;
        bottom: 0;
        width: 100%;
        background:url({{ asset('img/loader.gif') }}) no-repeat center center rgba(0,0,0,0.50);
        z-index: 99999;
    }
        .errorText{
            color: #cd1000;
            font-size: 12px;
        }
        .wp-width:{
            width: 25px;
        }
        @media screen and (min-device-width: 481px) and (max-device-width: 768px)
        {
            .wp-width{
                Width: 100%;
            }
        }
        .input-containersss {
            position: relative;
            }
            .currency-symbol {
            position: absolute;
            top: 54%;
            left: 7px;
            }
        th,td{
            color: #000;
            font-family: 'Poppins', sans-serif;
        }
        table{
            text-align: left;
        }
    </style>
</head>
<body style="background-color: #ffffff;">
<section  style="">
  <div class="row d-flex justify-content-center">
    <div class="col-md-12 col-lg-12 col-xl-12">
      <div class="card rounded-3" style="background: rgb(255 255 255);">
        <!-- <div class="card-body p-4" id="pdf-content"> -->
        <div class="card-body p-4" id="pdfContent">
            <div class="text-center">
                <img src="{{asset('images/tek-logo.png')}}" alt="" style="height: 70px;">
                 <h4 style="font-family: 'Poppins', sans-serif;">Payment Receipt</h4>
                 {{-- @php print_r($data); @endphp --}}
            </div>
            {{-- <div style="background-color: #2396f1; height:50px;">
                <h4 class="text-white" style="font-family: 'Poppins'; float:right;margin-top: 10px; margin-left:10px;">Payment Receipt</h4>
            </div> --}}
            <div class="text-center mb-4">
                @if($data['payment_status']=="paid")
                 <img class="mt-2 success-message" src="{{asset('img/payment-successful.png')}}" alt="" style="height: 120px;">
                 <h4 class="mt-2 success-message" style="color:#3bb54a;">Payment Successful!</h4>
                @endif
                 {{-- @php print_r($data); @endphp --}}
            </div>
            <div class="table-responsive">
             <table class="table">
                <tr style="background-color: #ffffff; height: 132px; padding-left:20px;">
                    <th colspan="3" style="padding-top: 52px;"><b>To: <br>{{ucfirst($data['first_name'])}} {{ucfirst($data['last_name'])}} </b><br>
                    <span> 
                        @if ($data['city'])
                            {{ ucfirst($data['city']) }},
                        @endif 
                        @if ($data['state'])
                         {{$data['state']}}.
                        @endif</span></th>
                    <th colspan="1" style="padding-top: 52px; "><b>Invoice #100{{$data['id']}} <br>Date: {{date('n/j/Y',strtotime($data['created_at']))}}</b></th>
                </tr>
                {{-- <tr style="background-color: #F3F2F1; height: 132px;">
                    <th colspan="2" style="padding-top: 52px;"><b>Date: {{date('n/j/Y g:i A',strtotime($data['created_at']))}} </b></th>
                    <th colspan="2" style="padding-top: 52px; padding-left: 286px;"><b style="background-color: #ffffff; padding:10px;">Invoice #100{{$data['id']}}</b></th>
                </tr> --}}
                <tr>
                    {{-- <th style="padding-top: 29px;"><b>Name:</b></th>
                    <td style="padding-top: 29px;"><b>{{$data['name']}}</b></td> --}}
                    <th style="padding-top: 29px;"><b>Amount:</b></th>
                    <td style="padding-top: 29px;"><b>${{number_format($data['amount'],2)}}</b></td>
                    <th style="padding-top: 29px;"><b>Payment Status:</b></th>
                    <td style="padding-top: 29px;"><b>{{strtoupper($data['payment_status'])}}</b></td>
                </tr>
                <tr>
                    <th><b>Email:</b></th>
                    <td><b>{{$data['email']}}</b></td>
                    <th><b>Contact:</b></th>
                    <td><b>{{ substr($data['phone'], -10, -7) . "-" . substr($data['phone'], -7, -4) . "-" . substr($data['phone'], -4) }}</b></td>
                </tr>
                <tr>

                    {{-- <th><b>Address:</b></th>
                    <td><b>{{$data['city']}}, {{$data['state']}}.</b></td> --}}
                    <th><b>Transaction ID:</b></th>

                    <td><b>{{$data['transcation_id']}}</b></td>
                    @if($data['payment_type']=="card")
                    <th><b>Card Number:</b></th>
                    <td><b>XXXX-XXXX-XXXX-{{substr($data['card_details'], -4)}}</b></td>
                    @else
                    <th><b>Account Number:</b></th>
                    <td><b>{{$data['card_details']}}</b></td>
                    @endif
                </tr>

                {{-- <tr>
                    <th class="text-center">Thank you for joining with us</th>
                </tr> --}}
             </table>
             </div>
             
             <div class="text-center text-dark  d-none" style="width:100%;" id="bottom-message">
                <p class="mt-3 mb-0" style="font-size:13px; font-weight:bold;">Thanks for doing business with  us!</p>
                {{-- <p class="mb-0" style="font-size:13px;">This invoice is computer-generated no signature is required!</p> --}}
                {{-- <h4 class="mb-0" style="font-size: 13px;">info@tekmatr.co</h4> --}}
             </div>
        </div>
            <div class="text-center" style="margin-top: 20px; margin-bottom: 10px;">
                <a id="download-pdf" class="btn btn-success">Download Invoice</a>
                @if($show=='1')
                <a id="continue-payment" href="{{route('home')}}" class="btn btn-success" style="margin-left: 10px;">Continue For Next Payment</a>                
                @endif
            </div>
      </div>
    </div>
  </div>
</section>
@php $invoice = $data['id'] @endphp
<script src="https://code.jquery.com/jquery-3.6.4.min.js"></script>

<!-- Include jsPDF -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/2.4.0/jspdf.umd.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/html2pdf.js/0.8.0/html2pdf.bundle.min.js" integrity="sha512-w3u9q/DeneCSwUDjhiMNibTRh/1i/gScBVp2imNVAMCt6cUHIw6xzhzcPFIaL3Q1EbI2l+nu17q2aLJJLo4ZYg==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<script>
const invoiceNumber = @json($invoice);
document.getElementById('download-pdf').addEventListener('click', function () {
    // Hide the download button
    const downloadButton = document.getElementById('download-pdf');
    downloadButton.style.display = 'none';
    
    const bottomMessage = document.getElementById('bottom-message');
    bottomMessage.classList.remove("d-none");
    const successMessage = document.getElementsByClassName('success-message');
    for (var i = 0; i < successMessage.length; i++) {
        successMessage[i].style.display = 'none';
    }

    // Get the full page content
    const fullPageContent = document.documentElement;

    // Customize styles for th and td elements
    const thElements = fullPageContent.querySelectorAll('th');
    const tdElements = fullPageContent.querySelectorAll('td');

    thElements.forEach(th => {
        th.style.fontSize = '11px';
    });

    tdElements.forEach(td => {
        td.style.fontSize = '11px';
    });

    // Set the options for html2pdf
    const options = {
        margin: 10,
        filename: 'invoice_1000'+invoiceNumber+'.pdf',
        image: { type: 'jpeg', quality: 0.98 },
        html2canvas: { scale: 2 },
        jsPDF: { unit: 'mm', format: 'a4', orientation: 'portrait' }
    };
    const pdfContent = document.getElementById('pdfContent');
    // Use html2pdf to convert and download the PDF
   // html2pdf(fullPageContent, options).then(() => {
    html2pdf(pdfContent, options).then(() => {
        // Show the download button again after PDF generation is complete
        downloadButton.style.display = 'block';

        // Reset styles for th and td elements (optional)
        thElements.forEach(th => {
            th.style.fontSize = ''; // Reset to default
        });

        tdElements.forEach(td => {
            td.style.fontSize = ''; // Reset to default
        });

    });
    //window.location.href = '/payment-list';
    // history.back();
});
 </script>

{{-- <script>
    $(document).ready(function () {
        $('#download-pdf').on('click', function () {
            // Create a jsPDF instance
            var pdf = new jsPDF();

            // Add the current page content to the PDF
            pdf.html(document.body, {
                callback: function () {
                    // Save the PDF file
                    pdf.save('document.pdf');
                }
            });
        });
    });
</script> --}}
{{-- <script>
    $(document).ready(function () {
        $('#download-pdf').on('click', function () {
            var htmlContent = $('#pdf-content').html();

            // Send the HTML content to the server for PDF generation
            $.ajax({
                url: '/download-pdf',
                type: 'POST',
                data: { html: htmlContent },
                success: function (data) {
                    // Trigger download
                    var blob = new Blob([data], { type: 'application/pdf' });
                    var link = document.createElement('a');
                    link.href = window.URL.createObjectURL(blob);
                    link.download = 'download.pdf';
                    link.click();
                }
            });
        });
    });
</script> --}}
{{-- <script>
     $(document).ready(function () {
            $('#download-pdf').on('click', function () {
                //alert('hello');
                var pdf = new jsPDF();
                alert(pdf);
                // Add the current page content to the PDF
                pdf.html(document.body, {
                    callback: function () {
                        // Save the PDF file
                        pdf.save('document.pdf');
                    }
                });
            });
        });
</script> --}}
{{-- <script>
    $(document).ready(function () {
            // Check if the page was reloaded
        if (performance.navigation.type === 1) {
            // Redirect to the home route
            window.location.replace("{{ route('home') }}");
        }
    });
</script> --}}
</body>
</html>
