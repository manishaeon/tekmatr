<!doctype html>
<html lang="en" dir="ltr">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>tek matr</title>
    <meta name="csrf-token" content="{{ csrf_token() }}" />


    <link rel="shortcut icon" href="{{ asset('images/tek-logo.png') }}" />

    <!-- Library / Plugin Css Build -->
    <link rel="stylesheet" href="{{ asset('css/core/libs.min.css') }}">

    <!-- Custom Css -->
    <link rel="stylesheet" href="{{ asset('css/aprycot.css') }}">
    <link rel="stylesheet" href="{{ asset('css/jquery-ui.min.css') }}">

    <script src="https://code.jquery.com/jquery-3.6.1.min.js"
        integrity="sha256-o88AwQnZB+VDvE9tvIXrMQaPlFFSUTR+nldQm1LuPXQ=" crossorigin="anonymous"></script>


    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.2.0/css/all.min.css"
        integrity="sha512-xh6O/CkQoPOWDdYTDqeRdPCVd1SpvCA9XXcUnZS2FmJNp1coAFzvtCN9BmamE+4aHK8yyUHUSCcJHgXloTyT2A=="
        crossorigin="anonymous" referrerpolicy="no-referrer" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.2.0/js/all.min.js"
        integrity="sha512-naukR7I+Nk6gp7p5TMA4ycgfxaZBJ7MO5iC3Fp6ySQyKFHOGfpkSZkYVWV5R7u7cfAicxanwYQ5D1e17EfJcMA=="
        crossorigin="anonymous" referrerpolicy="no-referrer"></script>

    {{-- Sweet Alert --}}
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <div id='loader'></div>
    <style>
    #loader {
        display: none;
        position: fixed;
        top: 0;
        left: 0;
        right: 0;
        bottom: 0;
        width: 100%;
        background:url({{ asset('img/loader.gif') }}) no-repeat center center rgba(0,0,0,0.50);
        background-size: 150px;
        z-index: 99999;
    }
        .errorText{
            color: #cd1000;
            font-size: 12px;
        }
        .wp-width:{
            width: 25px;
        }
        @media screen and (min-device-width: 481px) and (max-device-width: 768px)
        {
            .wp-width{
                Width: 100%;
            }
        }
        .input-containersss {
            position: relative;
            }
            .currency-symbol {
            position: absolute;
            top: 54%;
            left: 7px;
            }
    </style>
</head>
<body style="background-color: #e5e5e5;">
    <section class="p-4 p-md-12" style="">
  <div class="row d-flex justify-content-center">
    <div class="col-md-12 col-lg-12 col-xl-8">
      <div class="card rounded-3" style="background: rgb(255 255 255);">
        <div class="card-body p-4">
          <div class="text-center mb-4">
            <img src="{{asset('images/tek-logo.png')}}" alt="" style="height: 70px;">
          </div>
          <form action="{{route('add-ach-payment')}}" method="post" id="paymentform" novalidate>
            @csrf
            <div class="row">
            <div class="col-lg-12">
                @if (session('message'))
                    <div class="alert alert-success">
                        {{ session('message') }}
                    </div>
                @endif
                @if (session('error'))
                    <div class="alert alert-danger">
                        {{ session('error') }}
                    </div>
                @endif
            </div>
            <div class="col-md-6">
                <p class="fw-bold mb-1 pb-2">Account Details :</p>
                <div class="row">
                <div class="col-md-6 col-12">
                    <div class="form-outline mb-2 input-containersss">
                    <label class="form-label">Pay Amount ($) <span class="text-danger">*</span></label>
                    <input type="number" name="order_amount" class="form-control pricess form-control-md"
                    placeholder="0.00" style="border-radius:10px;" aria-label="Amount (to the nearest dollar)" step=".01" value="{{old('order_amount')}}" />
                    @if ($errors->has('order_amount'))
                        <span class="help-block errorText" style="font-size: 11px; white-space: nowrap;" >
                        {{ $errors->first('order_amount') }}
                        </span>
                    @endif
                    </div>
                </div>
            <div class="col-md-6 col-12">
            <div class="form-outline mb-3">
              <label class="form-label">Name <span class="text-danger">*</span></label>
              <input type="text" name="name"  class="form-control form-control-md"
              placeholder="Name" style="border-radius:10px;" value="{{old('name')}}" />
            @if ($errors->has('name'))
              <span class="help-block errorText">
              {{ $errors->first('name') }}
              </span>
            @endif
            </div>
            </div>
        </div>
            <div class="col-md-12 col-12">
                <div class="form-outline mb-3">
                  <label class="form-label">Account Number <span class="text-danger">*</span></label>
                  <input type="text" name="account_number" id="account_number" class="form-control numonly form-control-md"
                  placeholder="Account Number" style="border-radius:10px;" maxlength="17" minlength="5" />
                @if ($errors->has('account_number'))
                  <span class="help-block errorText" id="account_number">
                  {{ $errors->first('account_number') }}
                  </span>
                @endif
                </div>
              </div>
              <div class="col-md-12 col-12">
                <div class="form-outline mb-3">
                  <label class="form-label">Routing Number <span class="text-danger">*</span></label>
                  <input type="text" name="bank_code" class="form-control numonly form-control-md"
                  placeholder="Routing Number" style="border-radius:10px;" maxlength="9" />
                @if ($errors->has('bank_code'))
                  <span class="help-block errorText">
                  {{ $errors->first('bank_code') }}
                  </span>
                @endif
                </div>
              </div>
              <div class="col-md-12 col-12">
                <div class="form-outline mb-3">
                  <label class="form-label">Account Type <span class="text-danger">*</span></label>
                  <select class="form-control form-control-md" style="border-radius:10px;" name="account_type">
                    <option value="">Select Account Type</option>
                    <option value="SAVINGS">Savings accounts</option>
                    <option value="CHECKING">Checking accounts</option>
                  </select>
                @if ($errors->has('account_type'))
                  <span class="help-block errorText">
                  {{ $errors->first('account_type') }}
                  </span>
                @endif
                </div>
              </div>
            {{-- <div class="row mb-4">

              <div class="col-md-6 col-6">
                <div class="form-outline">
                  <label class="form-label">Expiration <span class="text-danger">*</span></label>
                  <input type="text" name="expiry" id="expiryData1" class="form-control expiryInput form-control-md"
                    placeholder="MM/YYYY" style="border-radius:10px;" onkeyup="formatString(event);" maxlength="7" />
                 @if ($errors->has('expiry'))
                    <span class="help-block errorText">
                    {{ $errors->first('expiry') }}
                    </span>
                  @endif
                </div>
              </div>
              <div class="col-md-6 col-6">
                <div class="form-outline">
                  <label class="form-label" for="formControlLgcvv">CVV <span class="text-danger">*</span></label>
                  <input type="password" class="form-control numonly form-control-md "
                    placeholder="***" name="cvv" style="border-radius:10px;" maxlength="3" />
                    @if ($errors->has('cvv'))
                    <span class="help-block errorText">
                    {{ $errors->first('cvv') }}
                    </span>
                    @endif
                </div>
              </div>
            </div> --}}


              </div>
              <div class="col-md-6">
                <div class="row mb-4">
                  <p class="fw-bold mb-1 pb-2">Billing Address :</p>
                 
                    <div class="col-md-6 col-12">
                      <div class="form-outline mb-3">
                        <label class="form-label">First Name <span class="text-danger">*</span></label>
                        <input type="text" name="first_name" value="{{old('first_name')}}" class="form-control form-control-md"
                          placeholder="First name" style="border-radius:10px;" />
                          @if ($errors->has('first_name'))
                          <span class="help-block errorText">
                          {{ $errors->first('first_name') }}
                          </span>
                          @endif
                      </div>
                    </div>
                    <div class="col-md-6 col-12">
                      <div class="form-outline mb-3">
                        <label class="form-label">Last Name <span class="text-danger">*</span></label>
                        <input type="text" name="last_name" value="{{old('last_name')}}" class="form-control form-control-md"
                        placeholder="Last name" style="border-radius:10px;" />
                        @if ($errors->has('last_name'))
                          <span class="help-block errorText">
                          {{ $errors->first('last_name') }}
                          </span>
                          @endif
                      </div>
                  </div>
                
                <div class="col-md-6 col-12">
                    <div class="form-outline mb-3">
                      <label class="form-label">Email <span class="text-danger">*</span></label>
                      <input type="email" name="email" value="{{old('email')}}" class="form-control form-control-md"
                      placeholder="Email" style="border-radius:10px;" />
                      @if ($errors->has('email'))
                        <span class="help-block errorText">
                        {{ $errors->first('email') }}
                        </span>
                        @endif
                    </div>
                </div>
                <div class="col-md-6 col-12">
                    <div class="form-outline mb-3">
                      <label class="form-label">Phone <span class="text-danger">*</span></label>
                      <input type="text" name="phone" id="phone" value="{{old('phone')}}" class="form-control contactdata form-control-md"
                      placeholder="123-456-7890" style="border-radius:10px;" maxlength="12" />
                    @if ($errors->has('phone'))
                      <span class="help-block errorText">
                      {{ $errors->first('phone') }}
                      </span>
                    @endif
                    </div>
                </div>
                <div class="col-md-6 col-12">
                <div class="form-outline mb-3">
                      <label class="form-label">State</label>
                      {{-- <input type="text" name="region" value="{{old('phone')}}" id="formControlLgXM" class="form-control form-control-md"
                        placeholder="Region" style="border-radius:10px;" /> --}}
                        <select class="form-control form-control-md" name="region" id="region" style="border-radius:10px;">
                            <option value="">Select State </option>
                            <option value="AL" {{ old('region') == 'AL' ? 'selected' : '' }}>AL</option>
                            <option value="AK" {{ old('region') == 'AK' ? 'selected' : '' }}>AK</option>
                            <option value="AZ" {{ old('region') == 'AZ' ? 'selected' : '' }}>AZ</option>
                            <option value="AR" {{ old('region') == 'AR' ? 'selected' : '' }}>AR</option>
                            <option value="CA" {{ old('region') == 'CA' ? 'selected' : '' }}>CA</option>
                            <option value="CO" {{ old('region') == 'CO' ? 'selected' : '' }}>CO</option>
                            <option value="CT" {{ old('region') == 'CT' ? 'selected' : '' }}>CT</option>
                            <option value="DE" {{ old('region') == 'DE' ? 'selected' : '' }}>DE</option>
                            <option value="FL" {{ old('region') == 'FL' ? 'selected' : '' }}>FL</option>
                            <option value="GA" {{ old('region') == 'GA' ? 'selected' : '' }}>GA</option>
                            <option value="HI" {{ old('region') == 'HI' ? 'selected' : '' }}>HI</option>
                            <option value="ID" {{ old('region') == 'ID' ? 'selected' : '' }}>ID</option>
                            <option value="IL" {{ old('region') == 'IL' ? 'selected' : '' }}>IL</option>
                            <option value="IN" {{ old('region') == 'IN' ? 'selected' : '' }}>IN</option>
                            <option value="IA" {{ old('region') == 'IA' ? 'selected' : '' }}>IA</option>
                            <option value="KS" {{ old('region') == 'KS' ? 'selected' : '' }}>KS</option>
                            <option value="KY" {{ old('region') == 'KY' ? 'selected' : '' }}>KY</option>
                            <option value="LA" {{ old('region') == 'LA' ? 'selected' : '' }}>LA</option>
                            <option value="ME" {{ old('region') == 'ME' ? 'selected' : '' }}>ME</option>
                            <option value="MD" {{ old('region') == 'MD' ? 'selected' : '' }}>MD</option>
                            <option value="MA" {{ old('region') == 'MA' ? 'selected' : '' }}>MA</option>
                            <option value="MI" {{ old('region') == 'MI' ? 'selected' : '' }}>MI</option>
                            <option value="MN" {{ old('region') == 'MN' ? 'selected' : '' }}>MN</option>
                            <option value="MS" {{ old('region') == 'MS' ? 'selected' : '' }}>MS</option>
                            <option value="MO" {{ old('region') == 'MO' ? 'selected' : '' }}>MO</option>
                            <option value="MT" {{ old('region') == 'MT' ? 'selected' : '' }}>MT</option>
                            <option value="NE" {{ old('region') == 'NE' ? 'selected' : '' }}>NE</option>
                            <option value="NV" {{ old('region') == 'NV' ? 'selected' : '' }}>NV</option>
                            <option value="NH" {{ old('region') == 'NH' ? 'selected' : '' }}>NH</option>
                            <option value="NJ" {{ old('region') == 'NJ' ? 'selected' : '' }}>NJ</option>
                            <option value="NM" {{ old('region') == 'NM' ? 'selected' : '' }}>NM</option>
                            <option value="NY" {{ old('region') == 'NY' ? 'selected' : '' }}>NY</option>
                            <option value="NC" {{ old('region') == 'NC' ? 'selected' : '' }}>NC</option>
                            <option value="ND" {{ old('region') == 'ND' ? 'selected' : '' }}>ND</option>
                            <option value="OH" {{ old('region') == 'OH' ? 'selected' : '' }}>OH</option>
                            <option value="OK" {{ old('region') == 'OK' ? 'selected' : '' }}>OK</option>
                            <option value="OR" {{ old('region') == 'OR' ? 'selected' : '' }}>OR</option>
                            <option value="PA" {{ old('region') == 'PA' ? 'selected' : '' }}>PA</option>
                            <option value="RI" {{ old('region') == 'RI' ? 'selected' : '' }}>RI</option>
                            <option value="SC" {{ old('region') == 'SC' ? 'selected' : '' }}>SC</option>
                            <option value="SD" {{ old('region') == 'SD' ? 'selected' : '' }}>SD</option>
                            <option value="TN" {{ old('region') == 'TN' ? 'selected' : '' }}>TN</option>
                            <option value="TX" {{ old('region') == 'TX' ? 'selected' : '' }}>TX</option>
                            <option value="UT" {{ old('region') == 'UT' ? 'selected' : '' }}>UT</option>
                            <option value="VT" {{ old('region') == 'VT' ? 'selected' : '' }}>VT</option>
                            <option value="VA" {{ old('region') == 'VA' ? 'selected' : '' }}>VA</option>
                            <option value="WA" {{ old('region') == 'WA' ? 'selected' : '' }}>WA</option>
                            <option value="WV" {{ old('region') == 'WV' ? 'selected' : '' }}>WV</option>
                            <option value="WI" {{ old('region') == 'WI' ? 'selected' : '' }}>WI</option>
                            <option value="WY" {{ old('region') == 'WY' ? 'selected' : '' }}>WY</option>
                        </select>
                    </div>
                    
                </div>
                <div class="col-md-6 col-12">
                    <div class="form-outline mb-3">
                      <label class="form-label">City</label>
                      <input type="text" name="city" value="{{old('city')}}" class="form-control form-control-md"
                        placeholder="City" style="border-radius:10px;" />
                        @if ($errors->has('city'))
                      <span class="help-block errorText">
                      {{ $errors->first('city') }}
                      </span>
                    @endif
                    </div>                    
                </div>
                <div class="col-md-6 col-12">
                  <div class="form-outline mb-3">
                        <label class="form-label">Street Address</label>
                        <input type="text" name="street_address" value="{{old('street_address')}}" class="form-control form-control-md"
                          placeholder="Street Address" style="border-radius:10px;" />
                          @if ($errors->has('street_address'))
                        <span class="help-block errorText">
                        {{ $errors->first('street_address') }}
                        </span>
                      @endif
                  </div>                    
                </div>
                <div class="col-md-6 col-12">
                  <div class="form-outline mb-3">
                      <label class="form-label" >Zip Code</label>
                      <input type="text" name="zip_code" value="{{old('zip_code')}}" class="form-control numonly form-control-md"
                        placeholder="Zip Code" style="border-radius:10px;" maxlength="5" />
                        @if ($errors->has('zip_code'))
                      <span class="help-block errorText">
                      {{ $errors->first('zip_code') }}
                      </span>
                    @endif
                    </div>
                   
                </div>
                
            </div>

              </div>

            </div>
            <div class="row col-md-6">
                <div class="col-md-6 col-6">
                    <button class="btn btn-success btn-md btn-block text-center" style="border-radius:10px; width:100%;" id="btnPayNow">Pay Now</button>
                </div>
                <div class="col-md-6 col-6">
                    
                    <a id="continue-payment" href="{{route('home')}}" class="btn btn-success btn-md btn-block text-center" style="border-radius:10px; width:100%;">Cancel</a>
                </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</section>
<script>
    $(function() {      
        $( "form" ).submit(function(e) {
          $('#btnPayNow').prop('disabled');
            $('#loader').show();

        });
    });
</script>
<style>
  .error{
    color:#cd1000;
    font-size: 12px;
  }
</style>
<script src="{{asset('js/jquery-ui.min.js')}}"></script>
<script src="{{asset('js/validate.min.js')}}"></script>
<script type="text/javascript">
       // $(document).ready(function() {
            // $("#paymentform").validate({
            //   rules: {
            //       order_amount: {
            //             required: true,
            //             number:true
            //       },
            //       name: {
            //             required: true
            //       },
            //       account_number: {
            //             required: true,
            //             number:true,
            //       },
            //       bank_code: {
            //             required: true,
            //             number:true,
            //       },
            //       account_type: {
            //             required: true,
            //       },
            //       first_name: {
            //             required: true
            //       },
            //       last_name: {
            //             required: true
            //       },
            //       email: {
            //             required: true,
            //             email:true
            //       },
            //       phone: {
            //             required: true
            //       },
            //     },
            //   messages: {
            //       order_amount: {
            //             required: "Enter Pay Amount",
            //             number: "Enter Number Only"
            //       },
            //       name: {
            //             required: "Enter Full name"
            //       },
            //       account_number: {
            //             required: "Enter Account Number",
            //             number: "Enter Number Only"
            //       },
            //       bank_code: {
            //             required: "Enter Routing Number",
            //             number: "Enter Number Only"
            //       },
            //       account_type: {
            //             required: "Select Account Type"
            //       },
            //       first_name: {
            //             required: "Enter First name"
            //       },
            //       last_name: {
            //             required: "Enter Last name"
            //       },
            //       email: {
            //             required: "Enter Email Id",
            //             email: "Enter Valid Email Id"
            //       },
            //       phone: {
            //             required: "Enter Phone Number"
            //       },
            //     },

            // });
       // });
    </script>
<script>
$(document).ready(function() {

    $('#phone').on('input', function () {
          // Remove non-numeric characters from the input
          let phone = $(this).val().replace(/\D/g, '');

          // Format the phone number with hyphens
          if (phone.length === 10) {
              phone = phone.replace(/(\d{3})(\d{3})(\d{4})/, '$1-$2-$3');
          }

          // Update the input field with the formatted phone number
          $(this).val(phone);
      });

      // Listen for the popstate event (back or forward navigation)
      window.addEventListener('popstate', function () {
          // Deactivate the pre-loader when navigating back
          $('#loader').hide();
      });


  // $(".card_data").on("input", function() {
  //   // Remove non-digit characters
  //   var cardNumber = $(this).val().replace(/\D/g, "");

  //   // Add a space every 4 digits
  //   var formattedCardNumber = "";
  //   for (var i = 0; i < cardNumber.length; i++) {
  //     if (i > 0 && i % 4 === 0) {
  //       formattedCardNumber += " ";
  //     }
  //     formattedCardNumber += cardNumber.charAt(i);
  //   }

  //   // Update the input value with the formatted card number
  //   $(this).val(formattedCardNumber);
  // });
});
</script>
<script>
    function formatString(e) {
  var inputChar = String.fromCharCode(event.keyCode);
  var code = event.keyCode;
  var allowedKeys = [8];
  if (allowedKeys.indexOf(code) !== -1) {
    return;
  }

  event.target.value = event.target.value.replace(
    /^([1-9]\/|[2-9])$/g, '0$1/' // 3 > 03/
  ).replace(
    /^(0[1-9]|1[0-2])$/g, '$1/' // 11 > 11/
  ).replace(
    /^([0-1])([3-9])$/g, '0$1/$2' // 13 > 01/3
  ).replace(
    /^(0?[1-9]|1[0-2])([0-9]{2})$/g, '$1/$2' // 141 > 01/41
  ).replace(
    /^([0]+)\/|[0]+$/g, '0' // 0/ > 0 and 00 > 0
  ).replace(
    /[^\d\/]|^[\/]*$/g, '' // To allow only digits and `/`
  ).replace(
    /\/\//g, '/' // Prevent entering more than 1 `/`
  );
}

</script>
<script>
    $(".pricess").blur(function(e){
    e.preventDefault();
    var val = parseFloat($(this).val());
    if(val){
        $(this).val(val.toFixed(2));
    }
    else
    {
        $(this).val();
    }
    });
</script>
<script type="text/javascript">
    $('.numonly').keypress(function(event) {
      if (event.which != 46 && (event.which < 47 || event.which > 59))
      {
          event.preventDefault();
          if ((event.which == 46) && ($(this).indexOf('.') != -1)) {
              event.preventDefault();
          }
      }
    });
</script>

<script>
    $(document).ready(function() {
      $('.contactdata').keypress(function(event) {
          var charCode = event.which || event.keyCode;
          if (charCode > 31 && (charCode < 48 || charCode > 57) && charCode != 45) {
          return false;
          } else {
          var len = $(this).val().length;
          var index = $(this).val().indexOf('-');
          if (index != -1 && charCode == 45) {
              return false;
          }
          if (len == 3 || len == 7) {
              $(this).val($(this).val() + '-');
          }
          }
      });
      });
  </script>
</body>
</html>
