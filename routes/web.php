<?php

use App\Http\Controllers\ExtraPaymentController;
use App\Http\Controllers\MerchantPaymentController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\DashboardController;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('migratefresh',function(){
    Artisan::call('migrate:fresh');
});

Route::get('/', function () {
    return view('home');
})->name('home');

Route::get('admin', function () {
    return view('admin.auth.login');
})->name('admin');

Route::post('login', [AuthController::class, 'login']);

// Route::get('/home',[MerchantPaymentController::class,'index'])->name('home');
Route::get('not-found',[MerchantPaymentController::class,'notFound'])->name('not-found');
Route::post('add-customer-payment',[MerchantPaymentController::class,'addCustomerPayment'])->name('add-customer-payment');
Route::post('add-ach-payment',[MerchantPaymentController::class,'addAchPayment'])->name('add-ach-payment');
Route::post('check-payment-method',[MerchantPaymentController::class,'checkPaymentMethod'])->name('check-payment-method');
Route::get('card-payment',[MerchantPaymentController::class,'cardPayment'])->name('card-payment');
Route::get('ach-payment',[MerchantPaymentController::class,'achPayment'])->name('ach-payment');
Route::get('invoice/{id}/{show}',[MerchantPaymentController::class,'invoice'])->name('invoice');
Route::post('/download-pdf', [MerchantPaymentController::class, 'downloadPdf'])->name('download-pdf');
Route::middleware('auth:admin')->group(function () {
    Route::get('dashboard',[DashboardController::class,'index'])->name('dashboard');
    Route::get('payment-list',[DashboardController::class,'paymentList'])->name('payment-list');
    Route::post('change-payment-status',[DashboardController::class,'changePaymentStatus'])->name('change-payment-status');
    Route::get('logout',[AuthController::class,'logout'])->name('logout');
});
