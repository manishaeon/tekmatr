<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Api\PaymentController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
A| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::get('/payment-list', [PaymentController::class,'paymentList'])->name('payment-list');
Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});
