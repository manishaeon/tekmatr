<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ExtraPayment extends Model
{
    use HasFactory;
    protected $fillable = [
        'name',
        'first_name',
        'last_name',
        'amount',
        'city',
        'state',
        'pin_code',
        'street_address',
        'transcation_id',
        'merchant_id',
        'email',
        'phone',
        'card_details',
        'payment_type',
        'payment_status',
        'batch_status'
    ];
}
