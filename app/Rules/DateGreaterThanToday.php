<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;
use Carbon\Carbon;

class DateGreaterThanToday implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
      // brk the entered date into month and year
      $parts = explode('/', $value);
      $month = (int)$parts[0];
      $year = (int)$parts[1];

       // Get the current date
       $currentDate = Carbon::now();

       // Create a Carbon instance for the entered date
       $enteredDate = Carbon::createFromDate($year, $month, 1)->endOfMonth();

       // Compare the entered date with the current date
       return $enteredDate->greaterThan($currentDate);
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'Invalid card Expiry Date.';
    }
}
