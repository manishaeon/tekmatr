<?php

namespace App\Exceptions;

use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Throwable;

class Handler extends ExceptionHandler
{

    public function render($request, Throwable $e)
    {
        if ($this->isHttpException($e)) {
            return $this->renderHttpException($e);
        } elseif ($this->isFatal($e)) {
            // Handle fatal error
            // Example: Log the error and return a response
            // Log::error($e->getMessage());
            // return response()->view('errors.500', [], 500);
        } else {
            return parent::render($request, $e);
        }
    }

    protected function isFatal(Throwable $e)
    {
        return $e instanceof \Error && strpos($e->getMessage(), 'Allowed memory') !== false;
        // Add other checks for fatal errors if needed
    }
    /**
     * A list of the exception types that are not reported.
     *
     * @var array<int, class-string<Throwable>>
     */
    // public function render($request, Throwable $e)
    // {
    //     if ($this->isHttpException($exception)) {
    //         if ($exception->getStatusCode() == 404) {
    //             return response()->view('errors.' . '404', [], 404);
    //         }

    //         if ($exception->getStatusCode() == 405) {
    //             return response()->view('errors.' . '405', [], 405);
    //         }
    //     }

    //     return parent::render($request, $e);
    // }
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array<int, string>
     */
    protected $dontFlash = [
        'current_password',
        'password',
        'password_confirmation',
    ];

    /**
     * Register the exception handling callbacks for the application.
     *
     * @return void
     */
    public function register()
    {
        $this->reportable(function (Throwable $e) {
            //
        });
    }
}
