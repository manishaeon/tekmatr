<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Support\Facades\Log;
use Validator;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Hash;
use App\Models\ExtraPayment;
use Illuminate\Validation\Rules\Password;
use App\Rules\DateGreaterThanToday;

use Response;
use Snappy;


class MerchantPaymentController extends Controller
{


    public function index()
    {
        return view('home');
    }
    public function cardPayment()
    {
        return view('card-payment');
    }

    public function achPayment()
    {
        return view('ach-payment');
    }
    public function checkPaymentMethod(Request $request)
    {
        if($request->payment_option=="ach_payment")
        {
            return redirect('ach-payment');
        }
        else{
            return redirect('card-payment');
        }
    }

    public function addAchPayment(Request $request)
    {        
        $this->achValidation($request->all())->validate();
        $phone = str_replace('-', '', $request->phone);
        $card_number = str_replace(' ', '', $request->card_number);
        $ex = explode('/',$request->expiry);
        $idenData = [
            "entity" => [
                'phone' => $phone,
                'first_name' => $request->first_name,
                'last_name'=> $request->last_name,
                'email' => $request->email,
                "personal_address" => [
                    "city" => $request->city,
                    "country" => "USA",
                    "region" => $request->region,
                    "line1" => $request->street_address,
                    "postal_code" => $request->zip_code
                ],
            ]
        ];
        $username = env('FINIX_USER_NAME');
        $password = env('FINIX_PASSWORD');
        // $username = 'USxd1EPH9DeJyefCS4ZTVcLe';
        // $password = 'a9ef553f-eaef-479c-816d-759e4c652ae2';
        $identityApiUrl = env('FINIX_URL')."/identities";
        $identityResponse = Http::withBasicAuth($username, $password)
            ->withHeaders([
                'Content-Type' => 'application/json'
            ])->post($identityApiUrl,$idenData);
        $identity_response = json_decode((string) $identityResponse->getBody());
        //dd($identityResponse->getStatusCode());
        //dd($identity_response);
        if($identityResponse->getStatusCode()==201)
        {
            $customerIdentity = $identity_response->id;
            $tokenData = [
                    'name' => $request->name,
                    'account_type' => $request->account_type,
                    "country" => "USA",
                    'bank_code'=> $request->bank_code,
                    'account_number' => $request->account_number,
                    'security_code' => $request->cvv,
                    'type'=> "BANK_ACCOUNT",
                    'identity' => $customerIdentity,
            ];
            $tokenApiUrl = env('FINIX_URL')."/payment_instruments";
            $tokenResponse = Http::withBasicAuth($username, $password)
            ->withHeaders([
                'Content-Type' => 'application/json'
            ])->post($tokenApiUrl,$tokenData);
            $token_response = json_decode((string) $tokenResponse->getBody());

            //dd($token_response);
            // $customerIds = str_replace(' ', '-', $request->name);
            // $lookupData = [
            //     "apiLoginID" => "211E8BD0",
            //     "apiKey" => "bb397db8-082a-4238-83eb-1925aba41edc",
            //     "customerID" => $customerIds,
            //     "deliveredBySeller" => false,
            //     "origin" => [
            //         "Address1" => "3759 SW Badger Ave",
            //         "City" => "Redmond",
            //         "State" => "OR",
            //         "Zip5" => "97756",
            //         "Address2" => "Suite 205",
            //     ],
            //     "destination" => [
            //         "Address1" => $request->street_address,
            //         "City" => $request->city,
            //         "State" => $request->region,
            //         "Zip5" => $request->zip_code,
            //         // "Address2" => "Suite 205",
            //     ],
            //     "cartItems" => [
            //         [
            //             "Index" => "0",
            //             "ItemID" => "transfer",
            //             "Price" => $request->order_amount,
            //             "Qty" => "1",
            //         ],
            //     ],
            //     "cartID" => null,
            // ];
            // $apiUrls = "https://api.taxcloud.net/1.0/TaxCloud/Lookup";
            // $lookUpResponse = Http::post($apiUrls, $lookupData);
            // // dd($lookUpResponse);
            // $lookup_response = json_decode((string) $lookUpResponse->getBody());
            // dd($lookup_response);
            // if($lookup_response->ResponseType == 3)
            // {
            //     $cartId = $lookup_response->CartID;
            //     $currentId = ExtraPayment::latest()->value('id');
            //     $getId = $currentId + 1;
            //     $orderId = "ordr000".$getId;
            //     $AuthorizedData = [
            //         "apiLoginID" => "211E8BD0",
            //         "apiKey" => "bb397db8-082a-4238-83eb-1925aba41edc",
            //         "customerID" => $customerIds,
            //         "cartID" => $cartId,
            //         "orderID" => $orderId,
            //         "dateAuthorized" => date('Y-m-d H:i:s')
            //     ];
            //     $AuthorizedResponse = Http::post("https://api.taxcloud.net/1.0/TaxCloud/Authorized", $AuthorizedData);
            //     $authorized_response = json_decode((string) $AuthorizedResponse->getBody());
            //     //dd($authorized_response);
            // }
            if($tokenResponse->getStatusCode()==201)
            {
                $sourceId = $token_response->id;
                $amount = $request->order_amount*100;
               // $merchant = "MUiPmhYquhEuzoreBask6NQB";
               $merchant=env('MERCHANT_NUMBER');
                $paymentData = [
                    'merchant' => $merchant,
                    'currency'=> "USD",
                    'amount' => $amount,
                    'source' => $sourceId
                ];
                $paymentApiUrl = env('FINIX_URL')."/transfers";
                $paymentResponse = Http::withBasicAuth($username, $password)
                ->withHeaders([
                    'Content-Type' => 'application/json'
                ])->post($paymentApiUrl,$paymentData);
                $payment_response = json_decode((string) $paymentResponse->getBody());
                //dd($paymentResponse->getStatusCode());

                if($paymentResponse->getStatusCode()==201)
                {
                    if($payment_response->state=="FAILED")
                    {
                        $storeData = [
                            'name'=> $request->card_holder_name,
                            'first_name'=> $request->first_name,
                            'last_name'=> $request->last_name,
                            'amount'=> $request->order_amount,
                            'city'=> $request->city,
                            'state'=> $request->region,
                            'pin_code'=> $request->zip_code,
                            'street_address'=> $request->street_address,
                            'transcation_id'=> $payment_response->id,
                            'merchant_id'=> $merchant,
                            'email'=> $request->email,
                            'phone'=> $phone,
                            'card_details'=> $request->account_number,
                            'payment_type'=>'ach',
                            'payment_status'=>'failed'
                        ];
                        $paymentData = ExtraPayment::create($storeData);
                     return redirect('ach-payment')->with('message',$payment_response->failure_message);
                    }
                    else if($payment_response->state=="SUCCEEDED"){
                         $storeData = [
                            'name'=> $request->card_holder_name,
                            'first_name'=> $request->first_name,
                            'last_name'=> $request->last_name,
                            'amount'=> $request->order_amount,
                            'city'=> $request->city,
                            'state'=> $request->region,
                            'pin_code'=> $request->zip_code,
                            'street_address'=> $request->street_address,
                            'transcation_id'=> $payment_response->id,
                            'merchant_id'=> $merchant,
                            'email'=> $request->email,
                            'phone'=> $phone,
                            'card_details'=> $request->account_number,
                            'payment_type'=>'ach',
                            'payment_status'=>'paid'
                        ];
                        $paymentData = ExtraPayment::create($storeData);
                        // if($authorized_response->ResponseType == 3)
                        // {
                        //     $CapturedData = [
                        //         "apiLoginID" => "211E8BD0",
                        //         "apiKey" => "bb397db8-082a-4238-83eb-1925aba41edc",
                        //         "orderID" => $orderId,
                        //     ];
                        //     $CapturedResponse = Http::post("https://api.taxcloud.net/1.0/TaxCloud/Captured", $CapturedData);
                        //     $captured_response = json_decode((string) $CapturedResponse->getBody());
                        //     //dd($captured_response);
                        // }
                        //dd($paymentData);
                        $id = $paymentData->id;
                        //dd($paymentData);
                        // return $this->invoice($id);
                        $enCoded = base64_encode($id);
                        return redirect()->route('invoice', ['id' => $enCoded]);
                        //  return redirect('ach-payment')->with('message','Payment Successful!');
                     }
                     else if($payment_response->state=="PENDING")
                     {
                        $storeData = [
                            'name'=> $request->card_holder_name,
                            'first_name'=> $request->first_name,
                            'last_name'=> $request->last_name,
                            'amount'=> $request->order_amount,
                            'city'=> $request->city,
                            'state'=> $request->region,
                            'pin_code'=> $request->zip_code,
                            'street_address'=> $request->street_address,
                            'transcation_id'=> $payment_response->id,
                            'merchant_id'=> $merchant,
                            'email'=> $request->email,
                            'phone'=> $phone,
                            'card_details'=> $request->account_number,
                            'payment_type'=>'ach',
                            'payment_status'=>'pending'
                        ];
                        $paymentData = ExtraPayment::create($storeData);
                        return redirect('ach-payment')->with('message','Payment Pending!');
                     }
                     else{
                         return redirect('ach-payment')->with('message','Please Try Again');
                     }
                    // if($payment_response)
                    // $storeData = [
                    //     'name'=> $request->card_holder_name,
                    //     'amount'=> $request->order_amount,
                    //     'city'=> $request->city,
                    //     'state'=> $request->region,
                    //     'pin_code'=> $request->zip_code,
                    //     'street_address'=> $request->street_address,
                    //     'transcation_id'=> $payment_response->id,
                    //     'merchant_id'=> $merchant
                    // ];
                    // ExtraPayment::create($storeData);
                    // return redirect('ach-payment')->with('message','Payment Successful!');
                }
                else{
                    $paymentMessage = $payment_response->_embedded->errors[0]->message;
                    return redirect('ach-payment')->with('error',$paymentMessage);
                }
            }
            else{
                $tokenErrorStatus = $token_response->_embedded->errors[0]->message;
                return redirect('ach-payment')->with('error',$tokenErrorStatus);
            }
        }
        else{
            return redirect('ach-payment')->with('error','Something went wrong.');
        }

    }
    protected function achValidation(array $data)
    {
        //dd($data['id']);
        return Validator::make($data, [
            'first_name' => 'required|regex:/^[a-zA-Z][a-zA-Z0-9\s\-]*$/',
            'last_name' => 'required|regex:/^[a-zA-Z][a-zA-Z0-9\s\-]*$/',
            'phone' => 'required|regex:/^\d{3}-\d{3}-\d{4}$/',
            'email' => 'required|email',
            'order_amount' => 'required|numeric|min:1',
            'name' => 'required|regex:/^[a-zA-Z][a-zA-Z0-9\s\-]*$/',
            'account_number' => 'required|digits_between:5,17',
            'bank_code' => 'required|digits:9',
            'account_type' => 'required',
            'region' => 'required',
            'city'=> 'nullable|string|regex:/^[a-zA-Z][a-zA-Z0-9\s\-]*$/',
            'zip_code'=> 'nullable|string',
            'street_address'=> 'nullable|string',
        ]);
    }
    public function addCustomerPayment(Request $request)
    {
        $this->validator($request->all())->validate();
        $phone = str_replace('-', '', $request->phone);
        $card_number = str_replace(' ', '', $request->card_number);
        $ex = explode('/',$request->expiry);
        $idenData = [
            "entity" => [
                'phone' => $phone,
                'first_name' => $request->first_name,
                'last_name'=> $request->last_name,
                'email' => $request->email,
                "personal_address" => [
                    "city" => $request->city,
                    "country" => $request->country,
                    "region" => $request->region,
                    "line1" => $request->street_address,
                    "postal_code" => $request->zip_code
                ],
            ]
        ];
        $username = env('FINIX_USER_NAME');
       // dd($username);
        $password = env('FINIX_PASSWORD');
        $identityApiUrl = env('FINIX_URL')."/identities";
        $identityResponse = Http::withBasicAuth($username, $password)
            ->withHeaders([
                'Content-Type' => 'application/json'
            ])->post($identityApiUrl,$idenData);
        $identity_response = json_decode((string) $identityResponse->getBody());
        //dd($identityResponse->getStatusCode());
        // dd($identity_response);
        if($identityResponse->getStatusCode()==201)
        {
            $customerIdentity = $identity_response->id;
            $tokenData = [
                    'name' => $request->card_holder_name,
                    'expiration_year' => $ex[1],
                    'number'=> $card_number,
                    'expiration_month' => $ex[0],
                    "address" => [
                        "city" => $request->city,
                        "country" => $request->country,
                        "region" => $request->region,
                        "line1" => $request->street_address,
                        "postal_code" => $request->zip_code
                    ],
                    'security_code' => $request->cvv,
                    'type'=> "PAYMENT_CARD",
                    'identity' => $customerIdentity,
            ];
            $tokenApiUrl = env('FINIX_URL')."/payment_instruments";
            $tokenResponse = Http::withBasicAuth($username, $password)
            ->withHeaders([
                'Content-Type' => 'application/json'
            ])->post($tokenApiUrl,$tokenData);
            $token_response = json_decode((string) $tokenResponse->getBody());

            //dd($tokenResponse->getStatusCode());
            //tax slab lookup api calls
            // $customerIds = str_replace(' ', '-', $request->card_holder_name);
            // $lookupData = [
            //     "apiLoginID" => "211E8BD0",
            //     "apiKey" => "bb397db8-082a-4238-83eb-1925aba41edc",
            //     "customerID" => $customerIds,
            //     "deliveredBySeller" => false,
            //     "origin" => [
            //         "Address1" => "3759 SW Badger Ave",
            //         "City" => "Redmond",
            //         "State" => "OR",
            //         "Zip5" => "97756",
            //         "Address2" => "Suite 205",
            //     ],
            //     "destination" => [
            //         "Address1" => $request->street_address,
            //         "City" => $request->city,
            //         "State" => $request->region,
            //         "Zip5" => $request->zip_code,
            //         // "Address2" => "Suite 205",
            //     ],
            //     "cartItems" => [
            //         [
            //             "Index" => "0",
            //             "ItemID" => "transfer",
            //             "Price" => $request->order_amount,
            //             "Qty" => "1",
            //         ],
            //     ],
            //     "cartID" => null,
            // ];
            // $apiUrls = "https://api.taxcloud.net/1.0/TaxCloud/Lookup";
            // $lookUpResponse = Http::post($apiUrls, $lookupData);
            // // dd($lookUpResponse);
            // $lookup_response = json_decode((string) $lookUpResponse->getBody());
            // // dd($lookup_response);
            // if($lookup_response->ResponseType == 3)
            // {
            //     $cartId = $lookup_response->CartID;
            //     $currentId = ExtraPayment::latest()->value('id');
            //     $getId = $currentId + 1;
            //     $orderId = "ordr000".$getId;
            //     $AuthorizedData = [
            //         "apiLoginID" => "211E8BD0",
            //         "apiKey" => "bb397db8-082a-4238-83eb-1925aba41edc",
            //         "customerID" => $customerIds,
            //         "cartID" => $cartId,
            //         "orderID" => $orderId,
            //         "dateAuthorized" => date('Y-m-d H:i:s')
            //     ];
            //     $AuthorizedResponse = Http::post("https://api.taxcloud.net/1.0/TaxCloud/Authorized", $AuthorizedData);
            //     $authorized_response = json_decode((string) $AuthorizedResponse->getBody());
            //     //dd($authorized_response);
            // }
            // $lookUpResponse = Http::post('https://api.taxcloud.net/1.0/TaxCloud/Lookup')
            // ->post($lookupData);
            // $lookup_response = json_decode((string) $lookUpResponse->getBody());
            // dd($lookUpResponse);
            if($tokenResponse->getStatusCode()==201)
            {
                $sourceId = $token_response->id;
                $amount = $request->order_amount*100;
                // $merchant = "MUiPmhYquhEuzoreBask6NQB";
                //$merchant = "MUqJLrYkjsLweCAsQRqcCDhd";
                $merchant=env('MERCHANT_NUMBER');
                $paymentData = [
                    'merchant' => $merchant,
                    'currency'=> "USD",
                    'amount' => $amount,
                    'source' => $sourceId
                ];
                $paymentApiUrl = env('FINIX_URL')."/transfers";
                $paymentResponse = Http::withBasicAuth($username, $password)
                ->withHeaders([
                    'Content-Type' => 'application/json'
                ])->post($paymentApiUrl,$paymentData);
                $payment_response = json_decode((string) $paymentResponse->getBody());
                //dd($payment_response);
                //dd($paymentResponse->getStatusCode());

                if($paymentResponse->getStatusCode()==201)
                {

                   if($payment_response->state=="FAILED")
                   {
                    $storeData = [
                        'name'=> $request->card_holder_name,
                        'first_name'=> $request->first_name,
                        'last_name'=> $request->last_name,
                        'amount'=> $request->order_amount,
                        'city'=> $request->city,
                        'state'=> $request->region,
                        'pin_code'=> $request->zip_code,
                        'street_address'=> $request->street_address,
                        'transcation_id'=> $payment_response->id,
                        'merchant_id'=> $merchant,
                        'email'=> $request->email,
                        'phone'=> $phone,
                        'card_details'=> $card_number,
                        'payment_type'=>'card',
                        'payment_status' => 'failed'
                    ];
                    $paymentData = ExtraPayment::create($storeData);
                    return redirect('card-payment')->with('message',$payment_response->failure_message);
                   }
                   else if($payment_response->state=="SUCCEEDED"){
                    //dd($authorized_response);
                    //    if($authorized_response->ResponseType == 3)
                    //     {
                    //         $CapturedData = [
                    //             "apiLoginID" => "211E8BD0",
                    //             "apiKey" => "bb397db8-082a-4238-83eb-1925aba41edc",
                    //             "orderID" => $orderId,
                    //         ];
                    //         $CapturedResponse = Http::post("https://api.taxcloud.net/1.0/TaxCloud/Captured", $CapturedData);
                    //         $captured_response = json_decode((string) $CapturedResponse->getBody());
                    //         //dd($captured_response);
                    //     }
                        $storeData = [
                            'name'=> $request->card_holder_name,
                            'first_name'=> $request->first_name,
                            'last_name'=> $request->last_name,
                            'amount'=> $request->order_amount,
                            'city'=> $request->city,
                            'state'=> $request->region,
                            'pin_code'=> $request->zip_code,
                            'street_address'=> $request->street_address,
                            'transcation_id'=> $payment_response->id,
                            'merchant_id'=> $merchant,
                            'email'=> $request->email,
                            'phone'=> $phone,
                            'card_details'=> $card_number,
                            'payment_type'=>'card',
                            'payment_status' => 'paid'
                        ];
                        $paymentData = ExtraPayment::create($storeData);

                        //dd($paymentData);
                        $id = $paymentData->id;
                        //dd($paymentData);
                        // return $this->invoice($id);
                        $enCoded = base64_encode($id);
                        return redirect()->route('invoice', ['id' => $enCoded,'show'=>'1']);

                        // return redirect()->route('invoice/',$id);
                        // $id = ExtraPayment::latest()->value('id');
                        // $exResult = ExtraPayment::find($id);
                        // //dd($id);
                        // $arrayData = $exResult->toArray();
                        // return redirect()->route('invoice');
                        //return  $this->invoice();
                        // return view('invoice', ['data' => $exResult]);
                        // dd($exResult);
                        // return redirect()->route('invoice', ['data' => $exResult]);
                        //return redirect('invoice',['data'=>$exResult]);
                        //return redirect('card-payment')->with('message','Payment Successful!');
                    }
                    else if($payment_response->state=="PENDING")
                    {
                        // $id = ExtraPayment::latest()->value('id');
                        // $this->invoice($id);
                        $storeData = [
                            'name'=> $request->card_holder_name,
                            'first_name'=> $request->first_name,
                            'last_name'=> $request->last_name,
                            'amount'=> $request->order_amount,
                            'city'=> $request->city,
                            'state'=> $request->region,
                            'pin_code'=> $request->zip_code,
                            'street_address'=> $request->street_address,
                            'transcation_id'=> $payment_response->id,
                            'merchant_id'=> $merchant,
                            'email'=> $request->email,
                            'phone'=> $phone,
                            'card_details'=> $card_number,
                            'payment_type'=>'card',
                            'payment_status' => 'pending'
                        ];
                        $paymentData = ExtraPayment::create($storeData);
                        return redirect('card-payment')->with('message','Payment Pending!');
                    }
                    else{
                        $storeData = [
                            'name'=> $request->card_holder_name,
                            'first_name'=> $request->first_name,
                            'last_name'=> $request->last_name,
                            'amount'=> $request->order_amount,
                            'city'=> $request->city,
                            'state'=> $request->region,
                            'pin_code'=> $request->zip_code,
                            'street_address'=> $request->street_address,
                            'transcation_id'=> $payment_response->id,
                            'merchant_id'=> $merchant,
                            'email'=> $request->email,
                            'phone'=> $phone,
                            'card_details'=> $card_number,
                            'payment_type'=>'card',
                            'payment_status' => 'failed'
                        ];
                        $paymentData = ExtraPayment::create($storeData);
                        return redirect('card-payment')->with('message','Payment Failed');
                    }
                }
                else{
                    $paymentMessage = $payment_response->_embedded->errors[0]->message;
                    return redirect('card-payment')->with('error',$paymentMessage);
                }
            }
            else{
                $tokenErrorStatus = $token_response->_embedded->errors[0]->message;
                return redirect('card-payment')->with('error',$tokenErrorStatus);
            }
        }
        else{
            
            return redirect('/')->with('error','Something went wrong.');
        }
        // dd($ex[1]);
    }

    protected function validator(array $data)
    {
        //dd($data['id']);
        return Validator::make($data, [
            'first_name' => 'required|regex:/^[a-zA-Z][a-zA-Z0-9\s\-]*$/',
            'last_name' => 'required|regex:/^[a-zA-Z][a-zA-Z0-9\s\-]*$/',
            'phone' => 'required|regex:/^\d{3}-\d{3}-\d{4}$/',
            'email' => 'required|email',
            'order_amount' => 'required|numeric|min:1',
            'card_holder_name' => 'required|regex:/^[a-zA-Z][a-zA-Z0-9\s\-]*$/',
            'card_number' => 'required',
            'expiry' => ['required', 'date_format:m/Y', new DateGreaterThanToday],
            'country' => 'required',            
            'cvv' => 'required',
            'city'=> 'nullable|string|regex:/^[a-zA-Z][a-zA-Z0-9\s\-]*$/',
            'zip_code'=> 'nullable|string',
            'street_address'=> 'nullable|string',
        ]);
    }

    public function invoice($id,$show)
    {
        //dd($show);
        try {
            $deCodedId = base64_decode($id);
            $exResult = ExtraPayment::find($deCodedId);
            $arrayData = $exResult->toArray();
            return view('invoice',['data'=>$arrayData,'show'=>$show]);
        }
        catch (\Throwable $e) {
            // Log the exception
            Log::error($e->getMessage());
            return view('errors.404');
        }
    }

    public function downloadPdf(Request $request)
    {
        // $html = $request->input('html');
        $htmlContent = $request->input('html');

        $pdf = Snappy::loadHTML($htmlContent);

        return $pdf->download('document.pdf');
    // return response($html)
    // ->header('Content-Type', 'application/pdf')
    // ->header('Content-Disposition', 'inline; filename=download.pdf');
    }

    public function notFound()
    {
        return view('404');
    }

}
