<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\ExtraPayment;
use Carbon\Carbon;


class DashboardController extends Controller
{
    public function index()
    {
        $merchant=env('MERCHANT_NUMBER');
        $today = Carbon::now()->toDateString();
        $currentWeek = ExtraPayment::where('payment_status','paid')
        ->where('merchant_id',$merchant)
        ->whereBetween('created_at',
        [Carbon::now()->startOfWeek(), Carbon::now()->endOfWeek()])
        ->sum('amount');

        $todayPayment = ExtraPayment::where('payment_status', 'paid')
        ->where('merchant_id',$merchant)
        ->whereDate('created_at','=',$today)
        ->sum('amount');

        $currentMonth = Carbon::now()->startOfMonth();
        $currentMonthTotalAmount = ExtraPayment::where('created_at', '>=', $currentMonth)
        ->where('merchant_id',$merchant)
        ->where('created_at', '<', $currentMonth->copy()->endOfMonth())
        ->sum('amount');

        $currentMonthTotalAmount = ExtraPayment::where('created_at', '>=', $currentMonth)
        ->where('merchant_id',$merchant)
        ->where('created_at', '<', $currentMonth->copy()->endOfMonth())
        ->where('payment_status', 'paid')
        ->sum('amount');
        
        $totalAmount = ExtraPayment::where('payment_status', 'paid')
        ->sum('amount');
        //dd($todayPayment);
        return view('admin.dashboard',['currentWeek'=>$currentWeek,'todayPayment'=>$todayPayment,'currentMonthTotalAmount'=>$currentMonthTotalAmount,'totalAmount'=>$totalAmount]);
    }
    public function paymentList()
    {
        $payment = ExtraPayment::orderBy('id','desc')->get();
        return view('admin.payment-list',['payment'=>$payment]);
    }

    public function changePaymentStatus(Request $request)
    {
       $id = $request->id;
       $batch_status = $request->status;
       //dd($request->all());
    //    $checkPayment = ExtraPayment::where('id',$id)->latest()->first();
    //    if($checkPayment == "")
    if($batch_status=='refund')
    {      
        $updateData = ExtraPayment::where('id',$id)->update([
        'batch_status' => $batch_status,
        'payment_status' => 'refunded'
       ]);
    }else
    {
        $updateData = ExtraPayment::where('id',$id)->update([
            'batch_status' => $batch_status,           
           ]);
    }
        $data['success'] = true;
        return response()->json($data);
        // $data = ExtraPayment::find($id);    
        // return response()->json(['data' => $data->toArray()]);
     

    }
}
