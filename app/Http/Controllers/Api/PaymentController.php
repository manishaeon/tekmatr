<?php

namespace App\Http\Controllers\Api;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\ExtraPayment;
use Validator;
use Illuminate\Support\Facades\Log;

class PaymentController extends Controller
{
    public $successStatus = 200;
    public $failStatus = 404;

    public function paymentList()
    {
        $data = ExtraPayment::orderBy('id','desc')->get();
        $response = [
            'status'=> 'success',
            'data' => $data
        ];
        return response()->json($response,200);
    }


}
