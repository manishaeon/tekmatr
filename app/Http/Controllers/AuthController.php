<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{
    public function login(Request $request){
        $this->validate($request, [
            'email'   => 'required|email',
            'password' => 'required|min:6'
        ]);
        // dd($request->all());
        $credentails = $request->except("_token");

        if($this->guard()->attempt($credentails,true)){
            return redirect('/dashboard');
        }else{
            return redirect()->back()->withInput()->withErrors(['email'=>['Invalid credentials.']]);
        }
    }

    public function logout()
    {
        $this->guard()->logout();
        return redirect('admin');
    }



    // defining auth  guard
    protected function guard()
    {
        return Auth::guard('admin');
    }
}
