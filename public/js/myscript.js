function deleteterritory(id)
{
    if(confirm('Are you to sure delete data?')){
                var url = 'deleteterritory/'+id
                $.ajax({
                    enctype: 'multipart/form-data',
                    url: url,
                    type:'GET',
                    contentType: false,
                    processData: false,
                    success:function(data)
                    {
                        if(data.success)
                        {
                            $('.table').load(location.href + " .table");
                            swal({
                                title: "Good job!",
                                text: "Data Deleted Successfully",
                                icon: "success",
                                button: "OK",
                            });
                        }
                        else
                        {
                            swal({
                                title: "OOPS !",
                                text: "Data not Deleted Successfully",
                                icon: "error",
                                button: "OK",
                            });
                        }
                    }
                })
            }
}

function deletehub(id)
{
    if(confirm('Are you to sure delete data?')){
                var url = 'deletehub/'+id
                $.ajax({
                    enctype: 'multipart/form-data',
                    url: url,
                    type:'GET',
                    contentType: false,
                    processData: false,
                    success:function(data)
                    {
                        if(data.success)
                        {
                            $('.table').load(location.href + " .table");
                            swal({
                                title: "Good job!",
                                text: "Data Deleted Successfully",
                                icon: "success",
                                button: "OK",
                            });
                        }
                        else
                        {
                            swal({
                                title: "OOPS !",
                                text: "Data not Deleted Successfully",
                                icon: "error",
                                button: "OK",
                            });
                        }
                    }
                })
            }
}

function deletevendor(id)
{
    if(confirm('Are you to sure delete data?')){
                var url = 'deletevendor/'+id
                $.ajax({
                    enctype: 'multipart/form-data',
                    url: url,
                    type:'GET',
                    contentType: false,
                    processData: false,
                    success:function(data)
                    {
                        if(data.success)
                        {
                            $('.table').load(location.href + " .table");
                            swal({
                                title: "Good job!",
                                text: "Data Deleted Successfully",
                                icon: "success",
                                button: "OK",
                            });
                        }
                        else
                        {
                            swal({
                                title: "OOPS !",
                                text: "Data not Deleted Successfully",
                                icon: "error",
                                button: "OK",
                            });
                        }
                    }
                })
            }
}

function deleteorder(id)
{
    if(confirm('Are you to sure delete data?')){
                var url = 'deleteorder/'+id
                $.ajax({
                    enctype: 'multipart/form-data',
                    url: url,
                    type:'GET',
                    contentType: false,
                    processData: false,
                    success:function(data)
                    {
                        if(data.success)
                        {
                            $('.table').load(location.href + " .table");
                            swal({
                                title: "Good job!",
                                text: "Data Deleted Successfully",
                                icon: "success",
                                button: "OK",
                            });
                        }
                        else
                        {
                            swal({
                                title: "OOPS !",
                                text: "Data not Deleted Successfully",
                                icon: "error",
                                button: "OK",
                            });
                        }
                    }
                })
            }

}

function deletedriver(id)
{
    if(confirm('Are you to sure delete data?')){
              var url = 'deletedriver/'+id
                $.ajax({
                    enctype: 'multipart/form-data',
                    url: url,
                    type:'GET',
                    contentType: false,
                    processData: false,
                    success:function(data)
                    {
                        if(data.success)
                        {
                            $('.table').load(location.href + " .table");
                            swal({
                                title: "Good job!",
                                text: "Data Deleted Successfully",
                                icon: "success",
                                button: "OK",
                            });
                        }
                        else
                        {
                            swal({
                                title: "OOPS !",
                                text: "Data not Deleted Successfully",
                                icon: "error",
                                button: "OK",
                            });
                        }
                    }
                })
            }
}

function deleteUser(id)
{
    if(confirm('Are you to sure delete data?')){
              var url = 'deleteUser/'+id
                $.ajax({
                    enctype: 'multipart/form-data',
                    url: url,
                    type:'GET',
                    contentType: false,
                    processData: false,
                    success:function(data)
                    {
                        if(data.success)
                        {
                            $('.table').load(location.href + " .table");
                            swal({
                                title: "Good job!",
                                text: "Data Deleted Successfully",
                                icon: "success",
                                button: "OK",
                            });
                        }
                        else
                        {
                            swal({
                                title: "OOPS !",
                                text: "Data not Deleted Successfully",
                                icon: "error",
                                button: "OK",
                            });
                        }
                    }
                })
            }
}


