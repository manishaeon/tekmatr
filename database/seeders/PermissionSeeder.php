<?php

namespace Database\Seeders;

use App\Models\Admin;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;
use Spatie\Permission\PermissionRegistrar;

class PermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        app()[PermissionRegistrar::class]->forgetCachedPermissions();

        Permission::create(['name' => 'territory.*','guard_name' => "admin"]);
        Permission::create(['name' => 'territory.view','guard_name' => "admin"]);
        Permission::create(['name' => 'territory.create','guard_name' => "admin"]);
        Permission::create(['name' => 'territory.edit','guard_name' => "admin"]);
        Permission::create(['name' => 'territory.delete','guard_name' => "admin"]);
		
		 Permission::create(['name' => 'hub.*','guard_name' => "admin"]);
        Permission::create(['name' => 'hub.view','guard_name' => "admin"]);
        Permission::create(['name' => 'hub.create','guard_name' => "admin"]);
        Permission::create(['name' => 'hub.edit','guard_name' => "admin"]);
        Permission::create(['name' => 'hub.delete','guard_name' => "admin"]);


        $superadminRole = Role::create(['guard_name' => "admin", "name" => "superadmin"]);
		$superadminRole->givePermissionTo('territory.*');
				$superadminRole->givePermissionTo('hub.*');



        $super_admin = Admin::create(
            [
                'name' => 'Test User',
                'email' => 'test@gmail.com',
                'status' => 1,
                'password' => Hash::make('12345678')
            ]
        );
        $super_admin->assignRole($superadminRole);
        
        $temple_creator = Role::create(['guard_name' => "admin", "name" => "admin"]);
    }
}
